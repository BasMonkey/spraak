# Spraak Webinterface 

![poster](./docs/BB-poster.png "Poster")

![intro](./docs/Spraak-trailer-BB-gif.gif "intro")

This Project is all about building a research web application based on the PRAAT software ([http://www.fon.hum.uva.nl/praat/](http://www.fon.hum.uva.nl/praat/)) and OpenSMILE software ([https://www.audeering.com/opensmile/](https://www.audeering.com/opensmile/)) In order of the UMC Utrecht and the UMC Groningen.

The web app is built on a Java backend, linked to a dynamic web user-interface. The web app can be used in the browser on your computer 

All code is kept up-to-date in this Git repository.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.


### Prerequisites

Things you need to install the software and how to install them:

* Tomcat server | [Tomcat 9 download page](https://tomcat.apache.org/download-90.cgi)

* Java JRE | [JRE Download page](https://www.oracle.com/technetwork/java/javase/downloads/index.html)

* JavaJDK | [JDK Download page](https://www.oracle.com/technetwork/java/javase/downloads/index.html)

* Python 3 | [Python3 download page](https://www.python.org/downloads/)

* Database such as | [Mysql](https://www.mysql.com/downloads/)

* An IDE such as JetBrain's [IntelliJ IDEA](https://www.jetbrains.com/idea/)
-just for editing and to get the server up

* A decent webbrowser such as [Mozilla Firefox](https://www.mozilla.org/firefox/download/)

* openSmile [openSmile](https://www.audeering.com/opensmile/)

* praat [praat_nogui](http://www.fon.hum.uva.nl/praat/download_linux.html)


#### Install Python packages for praat software:

* Open a terminal window *(cmd.exe on windows)*
```
python3 -m pip install SomePackage
```

* parselmouth
```
pip install parselmouth
```
* seaborn
```
pip install seaborn
```
* numpy
```
pip install numpy
```
* matplot
```
pip install matplot
```

#### Install opensmile:

* Download the opensmile-2.3.0.zip file and extract it to the data folder in the webapp folder

#### Install praat

*  download the praat6056_linux64nogui.tar.gz file and extract it to the data folder in the webapp folder

### Set up Spraak

Now everything is installed, we can set up the web application

* Step 1 set up the environment

  * Clone this repo

  * Start your IDE (we will use IntelliJ IDEA as example here)

  * Click on 'Open project'

  * Check 'Use auto-import' ,'Use default gradle wrapper' and select the Java software, which you have installed earlier, as JDK.

* Step 2 Connect Tomcat server to project

  _Linux, Windows:_
  
  * Go to file... (top left).

  * Click on 'Settings'.

  * Scroll to 'Build, Execution, Deployment' in the sidebar and unfold this section.

  * Go to 'Application Servers' and click the '+' icon to add Tomcat server by selecting the folder where you have extracted the Tomcat server.
  
  _macOS:_
  
  * Go to the IntelliJ menu in the menu bar (next to the ).
  
  * Click on 'Preferences'.
  
  * Scroll to 'Build, Execution, Deployment' in the sidebar and unfold this section.
  
  * Go to 'Application Servers' and click the '+' icon to add Tomcat server by selecting the folder where you have extracted the Tomcat server.

### Set up Spraak-API

* Step 1 set up the environment

  * Clone this repo [Spraak-API](https://bitbucket.org/marcelzandberg/spraak-api/src/master/)

  * Start your IDE (we will use IntelliJ IDEA as example here)

  * Click on 'Open project'

  * Check 'Use auto-import' ,'Use default gradle wrapper' and select the Java software, which you have installed earlier, as JDK.

* Step 2 Connect Tomcat server to project

  _Linux, Windows:_
  
  * Go to file... (top left).

  * Click on 'Settings'.

  * Scroll to 'Build, Execution, Deployment' in the sidebar and unfold this section.

  * Go to 'Application Servers' and click the '+' icon to add Tomcat server by selecting the folder where you have extracted the Tomcat server.
  
  _macOS:_
  
  * Go to the IntelliJ menu in the menu bar (next to the ).
  
  * Click on 'Preferences'.
  
  * Scroll to 'Build, Execution, Deployment' in the sidebar and unfold this section.
  
  * Go to 'Application Servers' and click the '+' icon to add Tomcat server by selecting the folder where you have extracted the Tomcat server.



## Deployment Spraak

* Step 1 - Set up the server

  * Go to edit configurations (top right in your IntelliJ IDEA window)

  * Select application server Tomcat

  * Set URL: ```http://localhost:8080/```

  * Set JRE as  ```Default```

  * **_If in the bottom left of the window a "fix" button appears, press it and check the ```.war(exploded)``` artifact_**

* Step 2 - Change application.properties filepaths

  * Open the file application.properties You can find it here in your cloned repo

  * Change the paths to wherever you have extracted the web application folder.
  
  * **NOTE: These paths must be absolute, full paths**
  
  * Example: 
  
    * change: 
    
	  ```openSmile.executable-path=/homes/mazandberg/lib/opensmile-2.3.0/```
	  
	 
    * to: 
    
       ```openSmile.executable-path=/spraak/data/opensmile-2.3.0/```
  
 * Step 3 - Start the application

  * Press the little green triangle in the upper right of your screen. This will start the webserver and thereby the application.

  


## Deployment Spraak-API

* Step 1 - Set up the server

  * Go to edit configurations (top right in your IntelliJ IDEA window)

  * Select application server Tomcat

  * Set URL: ```http://localhost:portyouwanttosetup"(8090)"/```

  * Set JRE as  ```Default```

  * **_If in the bottom left of the window a "fix" button appears, press it and check the ```.war(exploded)``` artifact_**

* Step 2 - Change database settings in application.properties 

  * Open the file application.properties You can find it here in your cloned repo

  * Change the paths to wherever you have extracted the web application folder.
  
  * **NOTE: These paths must be absolute, full paths**
  
  * Example: 
  
    * change: 
    
	  ```spring.jpa.hibernate.ddl-auto=none```
      ```spring.datasource.url=jdbc:mysql://localhost:3306/db_example```
      ```spring.datasource.username=springuser```
      ```spring.datasource.password=ThePassword```
      ```spring.jpa.show-sql=true```
	  
	 
    * to: 

      ```spring.jpa.hibernate.ddl-auto=none```
      ```spring.datasource.url=jdbc:mysql://localhost:3306/db_UMCG```
      ```spring.datasource.username=ADMIN_UMCG```
      ```spring.datasource.password=**********```
      ```spring.jpa.show-sql=true```
       
  
 * Step 3 - Start the application

  * Press the little green triangle in the upper right of your screen. This will start the API and thereby the application.

  * open your webbrowser and type the addres you configured, in this case: ```http://localhost:8080/```
  
 * Step 4 - acces the application via db user credentials

    * execute the file schema.sql and data.sql

    * we predefiend a user for you here. username = developer, password = secret.

    * In the app you can change your account and add new users/admins

## Server-setup

* to be implemted


## Server-deployment

* to be implemented

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management
* [Font Awesome 5](https://fontawesome.com) - Easy adding icons to the web app
* [JQuery](https://code.jquery.com/) - JavaScript library
* [Gradle dependencies]() - commons-fileupload and commons-io
* [Sweetalert](https://sweetalert.js.org/) - creates alerts
* [Croppie](https://foliotek.github.io/Croppie/) - cropping plugin
* [Thymeleaf](https://www.thymeleaf.org/) - modern server-side Java template engine
* [Spring](https://spring.io/) - End-to-end support for reactive & servlet based apps
* [Mysql](https://www.mysql.com/) - The world's most popular open source database


## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.


## Versioning

We will use [semver](http://semver.org/) for versioning. But since this is the initial release of this software, there aren't any versions available yet.


## Authors

* **Bas Kasemir** - *Initial work* - [Portfolio](https://bioinf.nl/~bhnkasemir) | [Bitbucket](https://bitbucket.org/BasMonkey)

* **Marcel Zandberg** - *Initial work* - [Portfolio](https://bioinf.nl/~mazandberg) | [Bitbucket](https://bitbucket.org/marcelzandberg)


## License

* This project is licensed under the MIT License.

* Parselmouth is released under the GNU General Public License, version 3 or later.

* A [manuscript introducing Parselmouth](https://ai.vub.ac.be/~yajadoul/jadoul_introducing-parselmouth_a-python-interface-to-praat.pdf) has been published in the Journal of Phonetics.

  > Jadoul, Y., Thompson, B., & de Boer, B. (2018). Introducing Parselmouth: A Python interface to Praat. *Journal of Phonetics*, *71*, 1-15. https://doi.org/10.1016/j.wocn.2018.07.001

* Spraak WebWebapp only exposes Praat and openSmile's existing functionality and implementation of algorithms. If you use our Spraak Webapp in your research and plan to cite it in a scientific publication, please do not forget to [*cite Praat*](http://www.fon.hum.uva.nl/praat/manual/FAQ__How_to_cite_Praat.html). and [*cite openSmile*](https://www.audeering.com/opensmile/)

  > Boersma, Paul & Weenink, David (2019). Praat: doing phonetics by computer [Computer program]. Version 6.0.46, retrieved 3 January 2019 from http://www.praat.org/

  > Florian Eyben, Felix Weninger, Florian Gross, Björn Schuller: “Recent Developments in openSMILE, the Munich Open-Source Multimedia Feature Extractor”, In Proc. ACM Multimedia (MM), Barcelona, Spain, ACM, ISBN 978-1-4503-2404-5, pp. 835-838, October 2013. doi:10.1145/2502081.2502224




## Acknowledgments

* Thanks to Alban Voppel of the UMC Utrecht for the nice explanation of his wishes.
* Inspiration from Stackoverflow, the WebBased and applicationdesign courses at the Hanze University of Applied Sciences.
* Trello helpt us bigtime with the structure of the project and setting up "what to do".

