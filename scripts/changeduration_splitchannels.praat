form chageduration_channels_cutaudio
   sentence directory /directory
endform


Create Strings as file list... list 'directory$'/*.wav
numberOfFiles = Get number of strings
for ifile to numberOfFiles
   select Strings list
   fileName$ = Get string... ifile
   Read from file... 'directory$'/'fileName$'
	sound_name$ = selected$ ("Sound")

	select Sound 'sound_name$'
	Extract part: 0.00, 900, "rectangular", 1, 0
	Extract all channels
	Save as WAV file: "'directory$'/'fileName$'"
	
	
	select all
	minus Strings list
	Remove

endfor
