form split_channels    
	sentence directory /directory
	sentence patientID patient
endform

Create Strings as file list... list 'directory$'/*.wav
numberOfFiles = Get number of strings

for ifile to numberOfFiles
   select Strings list
   fileName$ = Get string... ifile
   Read from file... 'directory$'/'fileName$'
	sound_name$ = selected$ ("Sound")
	
	select Sound 'sound_name$'
	Scale intensity... 60
	Save as WAV file: "'directory$'/'fileName$'"

	select all
	minus Strings list
	Remove

endfor
