#!/usr/bin/env python3
import parselmouth

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import sys
import argparse
import os.path


def parsecommandline():
    """
    gets the arguments and returns them in a list
    """
    parser = argparse.ArgumentParser(description='PRAAT software')

    #checks if the file exists
    def file_choices(choices, fname):
        if not os.path.isfile(fname):
            print("Oops! The file does not exist, give a existing file please and try again")
            sys.exit(1)
        ext = os.path.splitext(fname)[1][1:]
        if ext not in choices:
            print("\nOops! you didnt give me the right file: audio_file.wav, it needs the file extention .wav take a look at the usage and try again please")
            sys.exit(1)
        return fname

    parser.add_argument('-w', '--wav', help='wav-file path(/home/file_name.wav)', nargs='*', type=lambda s:file_choices(("wav"), s))
    parser.add_argument('-o', '--output', help='set the output location', default=['/homes/mazandberg/bitbucket/themaopdracht-thema-10/thema10praat/output/'], required=False, nargs='*')

    parser.print_help()

    args = parser.parse_args()


    #check if has value
    if args.wav:
        wav_file = args.wav
    else:
        print("Oops! you didnt give me the file: audio_file.wav, it needs the file extention .wav take a look at the usage and try again please")
        sys.exit(1)
    if args.output:
        output_location = args.output


    return wav_file, output_location

def draw_sounplot(snd,output_location):
    sns.set() # Use seaborn's default style to make attractive graphs

    # Plot nice figures using Python's "standard" matplotlib library

    plt.figure()
    plt.plot(snd.xs(), snd.values.T)
    plt.xlim([snd.xmin, snd.xmax])
    plt.xlabel("time [s]")
    plt.ylabel("amplitude")
    plt.savefig(output_location + "sound.png")
    plt.savefig(output_location + "sound.pdf")


def draw_spectrogram(spectrogram, dynamic_range=70):
    X, Y = spectrogram.x_grid(), spectrogram.y_grid()
    sg_db = 10 * np.log10(spectrogram.values)
    plt.pcolormesh(X, Y, sg_db, vmin=sg_db.max() - dynamic_range, cmap='afmhot')
    plt.ylim([spectrogram.ymin, spectrogram.ymax])
    plt.xlabel("time [s]")
    plt.ylabel("frequency [Hz]")


def draw_intensity(intensity):
    plt.plot(intensity.xs(), intensity.values.T, linewidth=3, color='w')
    plt.plot(intensity.xs(), intensity.values.T, linewidth=1)
    plt.grid(False)
    plt.ylim(0)
    plt.ylabel("intensity [dB]")


def draw_pitch(pitch):
    # Extract selected pitch contour, and
    # replace unvoiced samples by NaN to not plot
    pitch_values = pitch.selected_array['frequency']
    pitch_values[pitch_values==0] = np.nan
    plt.plot(pitch.xs(), pitch_values, 'o', markersize=5, color='w')
    plt.plot(pitch.xs(), pitch_values, 'o', markersize=2)
    plt.grid(False)
    plt.ylim(0, pitch.ceiling)
    plt.ylabel("fundamental frequency [Hz]")


def main():
    wav_file, output_location = parsecommandline()

    print("used sound file: ",wav_file[0])
    print("The results are in location: ",output_location[0])

    snd = parselmouth.Sound(wav_file[0])
    draw_sounplot(snd, output_location[0])

    intensity = snd.to_intensity()
    spectrogram = snd.to_spectrogram()
    plt.figure()
    draw_spectrogram(spectrogram)

    plt.twinx()
    draw_intensity(intensity)
    plt.xlim([snd.xmin, snd.xmax])
    plt.savefig(output_location[0] + "spectrogram.png")

    pitch = snd.to_pitch()
    # If desired, pre-emphasize the sound fragment before calculating the spectrogram
    pre_emphasized_snd = snd.copy()
    pre_emphasized_snd.pre_emphasize()
    spectrogram = pre_emphasized_snd.to_spectrogram(window_length=0.03, maximum_frequency=8000)
    plt.figure()
    draw_spectrogram(spectrogram)
    plt.twinx()
    draw_pitch(pitch)
    plt.xlim([snd.xmin, snd.xmax])
    plt.savefig(output_location[0] + "spectrogram_0.03.png")


if __name__ == "__main__":
    sys.exit(main())
