package nl.bioinf.spraak.storage;
/**********************************
 * Copyright 2020 Marcel Zandberg *
 **********************************/
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class DeleteFiles {

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);



    @Value("${results-path}")
    private String results;

    @Value("${praat.files}")
    private String praatFiles;


    public boolean deleteAudioFiles(String randomHash){
        boolean status = false;
        File audioFilesDir = new File(praatFiles+randomHash+"\\");
        //clean up the uneccesarry audio files for better server performance
        try {
            FileUtils.cleanDirectory(audioFilesDir);
            FileUtils.deleteDirectory(audioFilesDir);
            status = true;
            logger.log(Level.INFO, "deleted audio files");
        } catch (IOException e) {
            e.getMessage();
        }

        return status;
    }


    public boolean deleteResultFiles(String randomHash){
        boolean status = false;
        File resultFilesDir = new File(results+randomHash+"\\");
        //clean up the uneccesarry result files for better server performance
        try {
            FileUtils.cleanDirectory(resultFilesDir);
            FileUtils.deleteDirectory(resultFilesDir);
            status = true;
            logger.log(Level.INFO, "deleted result files");
        } catch (IOException e) {
            e.getMessage();
        }
        return status;
    }


    public boolean cleanUpStorage(){
        File audioFiles = new File(praatFiles);
        File resultFiles = new File(results);
        boolean status = false;
        try {

            logger.log(Level.INFO, "trying to delete old session");
            if(audioFiles.isDirectory()) {
                if (audioFiles.list().length > 0) {
                    FileUtils.deleteDirectory(audioFiles);
                }
                logger.log(Level.INFO,"Deleted audio files");
            }

            if(resultFiles.isDirectory()){
                if(resultFiles.list().length > 0){
                    FileUtils.deleteDirectory(resultFiles);
                }
                logger.log(Level.INFO,"Deleted result files");
            }


        }catch (NullPointerException | IOException e){
            e.getMessage();
        }finally {
            try{
                //make the new directory
                resultFiles.mkdir();
                audioFiles.mkdir();
                status = true;
                logger.log(Level.INFO, "cleaned up the server session folders");
            }catch (Exception e){
                e.getMessage();
            }
        }
        return status;
    }
}
