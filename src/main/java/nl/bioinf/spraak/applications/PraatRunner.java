package nl.bioinf.spraak.applications;

/**
 *  created by Marcel Zandberg
 */





import nl.bioinf.spraak.service.APIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.io.*;
import java.security.Principal;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Runs Praat.
 */
@Component
public class PraatRunner {

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);



    @Value("${praat.praatscript-path}")
    private String praatscriptPath;
    @Value("${praat.praatprogram-name}")
    private String praatProgramName;
    @Value("${praat.results-path}")
    private String praatResultsPath;
    @Value("${praat.files}")
    private String praatFiles;
    @Value("${results-path}")
    private String results;

    public boolean praatStatus;


    /**
     * The Api service.
     */
    @Autowired
    APIService apiService;

    /**
     * Command line runner.
     *
     * @param principal     the principal
     * @param auth          the auth
     * @throws SQLException the sql exception
     * @throws IOException  the io exception
     */
    public void commandLineRunner(Principal principal, Authentication auth, String praatscript, String randomHash, String preProcess, String outDir) throws IOException {
        Runtime rt = Runtime.getRuntime();
        Process pr = null;
        try {

            if(preProcess.equals("1")){
                pr = rt.exec(praatscriptPath + praatProgramName + " --run " + praatscriptPath + praatscript + " " +outDir);
                aliveStatus(pr.isAlive());
                logger.log(Level.INFO,"pre-processed the files");
            }
            else {
                //commandline code to run praat_nogui with change duration
                pr = rt.exec(praatscriptPath + praatProgramName + " --run " + praatscriptPath + praatscript + " " +outDir+" "+results+randomHash+"_"+principal.getName());
                aliveStatus(pr.isAlive());
                logger.log(Level.INFO,"Praat is running");
            }


        } catch (IOException e) {
            System.out.println(e.toString());
            logger.log(Level.WARNING, "could not parse the lines");
            e.printStackTrace();
            throw new IOException();
        }

        while (pr.isAlive()){
            aliveStatus(pr.isAlive());
        }
        aliveStatus(pr.isAlive());
    }

    public void extractChannel(Principal principal, Authentication auth, String outDir, int channel) throws IOException {
        Runtime rt = Runtime.getRuntime();
        Process pr = null;
        logger.log(Level.INFO, "cmd: "+ praatscriptPath+praatProgramName+" --run "+praatscriptPath+"praat_splitsen_left.praat "+praatFiles+outDir+"/channel_L/ "+praatFiles+outDir+"/extracted/ | "+ praatscriptPath+praatProgramName+" --run "+praatscriptPath+"praat_splitsen_right.praat "+praatFiles+outDir+"/channel_R/ "+praatFiles+outDir+"/extracted/");

        //TODO change command for unix or windows for bash
        pr = rt.exec(praatscriptPath+praatProgramName+" --run "+praatscriptPath+"praat_splitsen_left.praat "+praatFiles+outDir+"/channel_L/ "+praatFiles+outDir+"/extracted/ | "+ praatscriptPath+praatProgramName+" --run "+praatscriptPath+"praat_splitsen_right.praat "+praatFiles+outDir+"/channel_R/ "+praatFiles+outDir+"/extracted/");

        aliveStatus(pr.isAlive());
        logger.log(Level.INFO, "Exctracting channels");

        while (pr.isAlive()){
            aliveStatus(pr.isAlive());
        }
        aliveStatus(pr.isAlive());

        logger.log(Level.INFO, "extracting channels complete");
    }


    public void aliveStatus(boolean status){
        praatStatus = status;
    }
}


