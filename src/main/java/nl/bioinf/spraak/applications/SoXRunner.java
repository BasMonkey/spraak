package nl.bioinf.spraak.applications;

/**
 *  Created by Bas Kasemir
 */


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Runs SoX.
 */
@Component
public class SoXRunner {

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    @Value(value = "${sox.path}")
    private String soxPath;

    /**
     * Command line runner for SoX.
     *
     * @param inputFilePath   the path to the input file
     * @param outputFilePath   the path to the output file
     * @param valueToGet The value to get from SoX
     * @throws SQLException the sql exception
     * @throws IOException  the io exception
     */
    public String commandLineRunnerSox(String inputFilePath, String outputFilePath, String valueToGet) throws IOException {
        String output = "Error: nothing found";

        try {
            Runtime rt = Runtime.getRuntime();
            //commandline code to run praat_nogui with change duration
            logger.log(Level.INFO,"SoX is running over file " + inputFilePath);
            Process proc = rt.exec(soxPath + " " + inputFilePath + " " + outputFilePath + " stats");

            // Wait so the output will be available
            proc.waitFor();
            logger.log(Level.INFO,"SoX is done");

            // Get the standard input
            BufferedReader stdInput = new BufferedReader(new
                    InputStreamReader(proc.getInputStream()));

            // Get the standard error
            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(proc.getErrorStream()));

            // read the output from the command
            String s = null;
            while ((s = stdInput.readLine()) != null) {
                if (s.contains(valueToGet)) {
                    return s;
                }
            }

            // read the output from the command
            String e = null;
            while ((e = stdError.readLine()) != null) {
                if (e.contains(valueToGet)) {
                    return e;
                }
            }

        } catch (Exception e) {
            // Catch the exception
            logger.log(Level.WARNING, "Could not parse the lines");
            e.printStackTrace();
        }

        // If nothing has been found, or neither an error occurred, return the default error message
        logger.log(Level.WARNING, "Specified value (" + valueToGet + ") was not found in sox output");
        return output;
    }

}


