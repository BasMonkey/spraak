package nl.bioinf.spraak.controllers;
/**********************************
 * Copyright 2020 Marcel Zandberg *
 **********************************/

import nl.bioinf.spraak.models.User;
import nl.bioinf.spraak.service.LocaleService;
import nl.bioinf.spraak.service.UserService;
import nl.bioinf.spraak.storage.DeleteFiles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
@RequestMapping("/delete")
public class DeleteFilesController {

    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    private final LocaleService localeService;
    private final UserService userService;
    private final DeleteFiles deleteFiles;

    @Autowired
    public DeleteFilesController(LocaleService localeService, UserService userService, DeleteFiles deleteFiles) {
        this.localeService = localeService;
        this.userService = userService;
        this.deleteFiles = deleteFiles;
    }

    @GetMapping(path = "/audio")
    public void deleteAudioFiles(@RequestParam(name="randomHash") String randomHash, String username, Model model, Principal principal, HttpServletRequest request, HttpServletResponse response){
        username = principal.getName();
        User userData = userService.getUserByUserName(username, false);

        model.addAttribute("user", userData);

        localeService.setLocale(principal, response, request);

        boolean status;

        logger.log(Level.INFO,"Deleting audio files");
        status = deleteFiles.deleteAudioFiles(randomHash);
        logger.log(Level.INFO, "Deleted audio files: "+status);
    }

    @GetMapping(path = "/result")
    @ResponseBody
    public boolean deleteResultDir(@RequestParam(name = "session") String session,String username, Model model, Principal principal, HttpServletRequest request, HttpServletResponse response){
        username = principal.getName();
        User userData = userService.getUserByUserName(username, false);

        model.addAttribute("user", userData);

        localeService.setLocale(principal, response, request);

        boolean status;

        logger.log(Level.INFO,"Deleting result directory");
        status = deleteFiles.deleteResultFiles(session);
        logger.log(Level.INFO, "Deleted result directory: "+status);
        return status;
    }

}
