package nl.bioinf.spraak.controllers;

/**
 * Copyright 2019 Bas Kasemir
 */

import nl.bioinf.spraak.auth.JWTTokenGenerator;
import nl.bioinf.spraak.models.Institute;
import nl.bioinf.spraak.models.User;
import nl.bioinf.spraak.service.InstituteService;
import nl.bioinf.spraak.service.UserService;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static nl.bioinf.spraak.auth.SecurityConstants.HEADER_STRING;
import static nl.bioinf.spraak.auth.SecurityConstants.TOKEN_PREFIX;

/**
 * Token controller for test use.
 */
@RestController
@RequestMapping("/token")
public class TokenController {

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    /**
     * The Jwt token generator.
     */
    @Autowired
    JWTTokenGenerator jwtTokenGenerator;

    /**
     * Gets token.
     *
     * @param request   the request
     * @param response  the response
     * @param username  the username
     * @param principal the principal
     * @param auth      the auth
     * @return the token
     * @throws IOException the io exception
     */
    @GetMapping(path = "/get", produces = "application/json")
    public String getToken(HttpServletRequest request, HttpServletResponse response, String username, Principal principal, Authentication auth) throws IOException {

        // Generate a token
        //JWTTokenGenerator jwtTokenGenerator = new JWTTokenGenerator();

        String token = jwtTokenGenerator.generateToken(auth, null, null, null);

        logger.log(Level.INFO,"Generated a token");

        return "[ {\"token\": \""+token+"\"} ]";

    }
}
