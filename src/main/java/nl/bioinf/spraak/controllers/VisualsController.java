package nl.bioinf.spraak.controllers;
/**
 * Copyright 2020 Marcel Zandberg
 */

import nl.bioinf.spraak.applications.OpenSmileRunner;
import nl.bioinf.spraak.applications.PraatRunner;
import nl.bioinf.spraak.models.*;
import nl.bioinf.spraak.service.*;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystemException;
import java.security.Principal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;


@Controller
@RequestMapping("/visuals")
public class VisualsController {
    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    /**
     * The Locale service.
     */

    private final UserService userService;
    private final PraatscriptService praatscriptService;
    private final PraatRunner praatRunner;
    private final OpenSmileRunner openSmileRunner;
    private final LocaleService localeService;


    @Value("${results-path}")
    private String results;

    @Value("${praat.files}")
    private String praatFiles;

    @Value("${path.slash}")
    private String pathslash;


    /**
     * Instantiates a new Results controller.
     * @param userService the user service
     * @param praatscriptService
     * @param spraakService
     * @param praatRunner
     * @param openSmileRunner
     * @param localeService
     */
    @Autowired
    public VisualsController(UserService userService, PraatscriptService praatscriptService, SpraakService spraakService, PraatRunner praatRunner, OpenSmileRunner openSmileRunner, LocaleService localeService) {
        this.userService = userService;
        this.praatscriptService = praatscriptService;
        this.praatRunner = praatRunner;
        this.openSmileRunner = openSmileRunner;
        this.localeService = localeService;
    }

    /**
     * Show results string.
     *
     * @param username  the username
     * @param model     the model
     * @param principal the principal
     * @param request   the request
     * @param response  the response
     * @return the string
     */
    @GetMapping
    public String showResults(String username, Model model, Principal principal, HttpServletRequest request, HttpServletResponse response) throws IOException {
        username = principal.getName();
        User userData = userService.getUserByUserName(username, false);
        List<Praatscript> praatscripts = praatscriptService.getAllPraatscripts();
        model.addAttribute("user", userData);
        model.addAttribute("praatscripts", praatscripts);

        localeService.setLocale(principal, response, request);

        logger.log(Level.INFO,"showing visuals page");


        return "visuals";
    }



    @PostMapping("/uploadVisuals")
    @ResponseBody
    public String handleUpload(@RequestParam("usePraat") String usePraat,
                               @RequestParam("useOpensmile") String useOpensmile ,
                               @RequestParam("fileAmount") String[] fileAmount ,
                               @RequestParam("preproces") String preproces,
                               @RequestParam("praatscript") String praatscript,
                               @RequestParam("randomHash") String randomHash,
                               @RequestParam("channelL") String channelL,
                               @RequestParam("channelR") String channelR,
                               RedirectAttributes redirectAttributes, @Valid Principal principal, Authentication auth) throws IOException, SQLException {

        //calls a new thread for the programs
        ExecutorService service = Executors.newFixedThreadPool(4);

        //audio file dir
        File audioFilesDir = new File(praatFiles+randomHash+pathslash);

        //audio preprocess file dir
        File audioPreProcesFilesDir = new File(praatFiles+randomHash+pathslash+"temp"+pathslash);

        //create the result dir
        File resultDir = new File(results+randomHash+"_"+principal.getName()+pathslash);
        resultDir.mkdir();


        File dirL = new File(praatFiles + randomHash+pathslash+"channel_L"+pathslash);
        File dirR = new File(praatFiles + randomHash+pathslash+"channel_R"+pathslash);
        File extracted = new File(praatFiles + randomHash+pathslash+"extracted"+pathslash);

        int channel = 1;
        int dirLenght;
        String outDir = "";

        //preprocessing audiofile
        if(preproces.equals("1")) {
            boolean preExist = false;

            while (!preExist) {
                try {
                    if (audioFilesDir.list().length == fileAmount.length) {
                        //temp dir for preprocessing
                        preProcesAudio(preproces, praatscript, randomHash, principal, auth, service, extracted);
                        preExist = true;
                    }
                }catch (NullPointerException e){
                    e.getMessage();
                    }
            }
        }else{
            boolean extract = false;
            while (!extract){
                try{
                    if(dirR.list().length == Integer.parseInt(channelR) && dirL.list().length == Integer.parseInt(channelL)){
                        extractChannel(principal, auth, service, randomHash, channel);
                        extract = true;
                    }
                }catch (NullPointerException e){
                    e.getMessage();
                }
            }
        }




        boolean exist = false;
        while (!exist) {
            try {
                if(!preproces.equals("1")){
                    dirLenght = extracted.list().length;
                    outDir = extracted.getPath();
                }else {
                    dirLenght = audioFilesDir.list().length;
                    outDir = audioPreProcesFilesDir.getPath();
                }
                //checks if the files are present
                if (dirLenght == fileAmount.length && !praatRunner.praatStatus) {
                    //praat runner in threadpool
                    if (usePraat.equals("1")) {
                        praatRunner(randomHash, principal, auth, service, outDir);
                    }

                    //opensmile runner in threadpool
                    if (useOpensmile.equals("1")) {
                        openSmileRunner(randomHash, principal, auth, service, outDir);
                    }
                    exist = true;
                }
            }catch (NullPointerException e){
                e.getMessage();
            }

        }


        //loader (if resultfiles are present and )
        boolean existResult = false;
        String loader = "";
        long startTime = System.currentTimeMillis();
        while (!existResult){

            //when praat is selected, check if program is done
            if (usePraat.equals("1") && !useOpensmile.equals("1") && resultDir.list().length > 0 && !praatRunner.praatStatus){
                loader = "Uploaded";
                existResult = true;
            }

            //when opensmile is selected, check if program is done
            if (useOpensmile.equals("1") && !usePraat.equals("1") && resultDir.list().length > (fileAmount.length -1) && !openSmileRunner.openSmileStatus){
                loader = "Uploaded";
                existResult = true;
            }

            //when opensmile and praat are selected, check if the programs are done
            if (useOpensmile.equals("1") && usePraat.equals("1") && resultDir.list().length > fileAmount.length && !openSmileRunner.openSmileStatus && !praatRunner.praatStatus){
                loader = "Uploaded";
                existResult = true;
            }

            //check when no programs exist
            if (!useOpensmile.equals("1") && !usePraat.equals("1") && resultDir.list().length == 0){
                loader = "no_programs";
                existResult = true;
            }

            //time out time = 100 sec
            if ((System.currentTimeMillis() - startTime) > 10000000){
                loader = "error!";
                existResult = true;
            }
        }

        redirectAttributes.addFlashAttribute("message",
                "You successfully uploaded: !");

        logger.log(Level.INFO, "submit form returns: "+ loader);

        return loader;
    }


    /**
     *commandline runners in thread
     */
    private void preProcesAudio(@RequestParam("preproces") String preproces, @RequestParam("praatscript") String praatscript, @RequestParam("randomHash") String randomHash, @Valid Principal principal, Authentication auth, ExecutorService service, File audioFilesDir) {
        File tempDir = new File(praatFiles+randomHash+"\\temp\\");
        tempDir.mkdir();
        service.submit(new Runnable() {
            public void run() {
                try {
                    logger.log(Level.INFO,"pre-processing the files");
                    praatRunner.commandLineRunner(principal, auth, praatscript, randomHash, preproces, audioFilesDir.getPath());
                } catch (IOException e) {
                    service.shutdownNow();
                    e.printStackTrace();
                }
            }
        });
    }

    private void openSmileRunner(@RequestParam("randomHash") String randomHash, @Valid Principal principal, Authentication auth, ExecutorService service, String outDir) {
        service.submit(new Runnable() {
            public void run() {
                try {
                    logger.log(Level.INFO, "Using opensmile");
                    openSmileRunner.commandLineRunner(principal, auth, randomHash, outDir);
                } catch (IOException e) {
                    service.shutdownNow();
                    e.printStackTrace();
                }
            }
        });
    }

    private void praatRunner(@RequestParam("randomHash") String randomHash, @Valid Principal principal, Authentication auth, ExecutorService service, String outDir) {
        String preprocesSetZero = "";
        service.submit(new Runnable() {
            public void run() {
                try {
                    String resultPraatscript = "praat_script.praat -25 2 0.3 0";

                    logger.log(Level.INFO, "Using praat");
                    praatRunner.commandLineRunner(principal, auth, resultPraatscript, randomHash, preprocesSetZero, outDir);
                } catch (IOException e) {
                    service.shutdownNow();
                    e.printStackTrace();
                }
            }
        });
    }

    private void extractChannel(@Valid Principal principal, Authentication auth, ExecutorService service, String outDir, int channel) {
        service.submit(new Runnable() {
            public void run() {
                try {
                    logger.log(Level.INFO, "Using praat");
                    praatRunner.extractChannel(principal, auth, outDir, channel);
                } catch (IOException e) {
                    service.shutdownNow();
                    e.printStackTrace();
                }
            }
        });
    }

}
