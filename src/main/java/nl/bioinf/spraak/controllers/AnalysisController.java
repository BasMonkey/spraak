package nl.bioinf.spraak.controllers;

/**
 *  created by Marcel Zandberg
 */

import nl.bioinf.spraak.applications.OpenSmileRunner;
import nl.bioinf.spraak.applications.PraatRunner;
import nl.bioinf.spraak.models.*;
import nl.bioinf.spraak.service.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Analysis controller serves analysis.
 */
@Controller
public class AnalysisController {

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    private final UserService userService;
    private final SpraakService spraakService;
    private final PraatRunner praatRunner;
    private final OpenSmileRunner openSmileRunner;
    private final LocaleService localeService;
    private final PraatscriptService praatscriptService;

    /**
     * Instantiates a new Analysis controller.
     *
     * @param userService     the user service
     * @param spraakService   the spraak service
     * @param praatRunner     the praat runner
     * @param openSmileRunner the open smile runner
     * @param localeService   the locale service
     */
    @Autowired
    public AnalysisController(UserService userService, SpraakService spraakService, PraatRunner praatRunner, OpenSmileRunner openSmileRunner, LocaleService localeService, PraatscriptService praatscriptService) {
        this.userService = userService;
        this.spraakService = spraakService;
        this.praatRunner = praatRunner;
        this.openSmileRunner = openSmileRunner;
        this.localeService = localeService;
        this.praatscriptService = praatscriptService;
    }

    /**
     * Show analysis string.
     *
     * @param request   the request
     * @param response  the response
     * @param username  the username
     * @param model     the model
     * @param principal the principal
     * @return the string
     */
    @GetMapping("/analysis")
    public String ShowAnalysis(HttpServletRequest request, HttpServletResponse response, String username, Model model, Principal principal) {
        logger.log(Level.INFO, "Analysis controller got a GET request");
        username = principal.getName();
        User userData = userService.getUserByUserName(username, false);

        localeService.setLocale(principal, response, request);

        List<Praatscript> praatscripts = praatscriptService.getAllPraatscripts();

        model.addAttribute("user", userData);
        model.addAttribute("praatscripts", praatscripts);

        return "analysis";
    }

    /**
     * Clear analysis string.
     *
     * @param request     the request
     * @param response    the response
     * @param spraakInput the spraak input
     * @param username    the username
     * @param model       the model
     * @param principal   the principal
     * @return the string
     */
    @GetMapping("/clearanalysismodel")
    @ResponseBody
    public String ClearAnalysis(HttpServletRequest request, HttpServletResponse response, SpraakInput spraakInput, String username, Model model, Principal principal) {
        logger.log(Level.INFO, "clearanalysismodel controller got a GET request");
        username = principal.getName();
        User userData = userService.getUserByUserName(username, false);

        localeService.setLocale(principal, response, request);
        //emptys the model
        spraakInput.setDate(null);
        spraakInput.setFileName(null);
        spraakInput.setPatNumber(null);
        spraakInput.setPraat_advanced(null);
        spraakInput.setRandomHash(null);
        spraakInput.setCutAudio(null);
        spraakInput.setNormalizeAudio(null);
        spraakInput.setTypeScript(null);

        return "cleared";
    }

}