package nl.bioinf.spraak.controllers;

/**
 *  created by Bas Kasemir
 */

import nl.bioinf.spraak.models.User;
import nl.bioinf.spraak.service.LocaleService;
import nl.bioinf.spraak.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
@RequestMapping("/health")
public class HealthController {

    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    private final UserService userService;

    private final LocaleService localeService;

    @Autowired
    public HealthController(UserService userService, LocaleService localeService) {
        this.userService = userService;
        this.localeService = localeService;
    }

    @GetMapping
    public String showAppApp(HttpServletRequest request, HttpServletResponse response, String username, Model model, Principal principal) {
        logger.log(Level.INFO, "Health page is requested");
        username = principal.getName();
        User userData = userService.getUserByUserName(username, false);

        localeService.setLocale(principal, response, request);

        model.addAttribute("user", userData);

        return "health";
    }

}