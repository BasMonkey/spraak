package nl.bioinf.spraak.controllers;

/**
 *  Created by Bas Kasemir
 */

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.TokenExpiredException;
import nl.bioinf.spraak.auth.JWTTokenGenerator;
import nl.bioinf.spraak.models.Institute;
import nl.bioinf.spraak.models.LoginAttempts;
import nl.bioinf.spraak.models.User;
import nl.bioinf.spraak.models.UserNew;
import nl.bioinf.spraak.service.EmailService;
import nl.bioinf.spraak.service.InstituteService;
import nl.bioinf.spraak.service.LocaleService;
import nl.bioinf.spraak.service.UserService;
import org.apache.http.auth.AUTH;
import org.hibernate.event.spi.ClearEventListener;
import org.jboss.aerogear.security.otp.Totp;
import org.jboss.aerogear.security.otp.api.Base32;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * User controller serves user information.
 */
@Configuration
@PropertySource(value={"classpath:application.properties","classpath:locale/messages.properties","classpath:locale/messages_nl.properties"})
@Controller
@RequestMapping("/user")
public class UserController {

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    // Get the host from the application.properties
    @Value("${application.host}")
    String appHost;

    // Get the English email subject
    @Value("${mail.new_account.subject.en}")
    String mail_new_account_subject_en;

    // Get the English email string
    @Value("${mail.new_account.en}")
    String mail_new_account_en;

    // Get the English email string
    @Value("${mail.new_account.username.en}")
    String mail_new_account_username_en;

    // Get the English email string
    @Value("${mail.activate.en}")
    String mail_activate_en;

    // Get the English email string
    @Value("${mail.reset.subject.en}")
    String mail_reset_subject_en;

    // Get the English email string
    @Value("${mail.reset.subject.en}")
    String mail_reset_body_en;

    // Get the English email string
    @Value("${mail.reset.not_you.en}")
    String mail_reset_not_you_en;

    // Get the Dutch email subject
    @Value("${mail.new_account.subject.nl}")
    String mail_new_account_subject_nl;

    // Get the Dutch email string
    @Value("${mail.new_account.nl}")
    String mail_new_account_nl;

    // Get the Dutch email string
    @Value("${mail.new_account.username.nl}")
    String mail_new_account_username_nl;

    // Get the Dutch email string
    @Value("${mail.activate.nl}")
    String mail_activate_nl;

    // Get the Dutch email string
    @Value("${mail.reset.subject.nl}")
    String mail_reset_subject_nl;

    // Get the Dutch email string
    @Value("${mail.reset.body.nl}")
    String mail_reset_body_nl;

    // Get the Dutch email string
    @Value("${mail.reset.not_you.nl}")
    String mail_reset_not_you_nl;

    // Get the general email footer string
    @Value("${mail.general.footer}")
    String mail_general_footer;

    @Autowired
    private EmailService emailService;

    private final UserService userService;

    /**
     * The Jwt token generator.
     */
    @Autowired
    JWTTokenGenerator jwtTokenGenerator;

    /**
     * The Locale service.
     */
    @Autowired
    LocaleService localeService;

    /**
     * The Institute service.
     */
    @Autowired
    InstituteService instituteService;

    /**
     * Instantiates a new User controller.
     *
     * @param userService the user service
     */
    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Retrieves information of given username.
     *
     * @param username_profile the username to be looked up
     * @param model            the model
     * @param principal        the principal
     * @param request          the request
     * @param response         the response
     * @return the string
     */
    @RequestMapping("/view/{username}")
    public String geneByAbbreviation(
            @PathVariable("username") String username_profile,
            Model model, Principal principal, HttpServletRequest request, HttpServletResponse response) {

        // Set the locale for the user
        localeService.setLocale(principal, response, request);

        // Create an empty user object
        User profile = null;

        // try to look up a user by its username
        try {
            profile = userService.getUserByUserName(username_profile, false);
        } catch (Exception e) {
            // Somehow the page needs at least the role of the user, so .
            profile = new User();
            profile.setRole("ROLE_USER");
            logger.log(Level.WARNING, "No user with username "+username_profile+" found");
        }

        // Bind the user to the response
        model.addAttribute("user_profile", profile);

        // Make a list of the institutes (for editing the user) and add it to the response
        List<Institute> instituteList = instituteService.getInstitutes(false);
        model.addAttribute("institutes", instituteList);

        // Add the user details of the logged in user
        String username = principal.getName();
        User loggedInUser = userService.getUserByUserName(username, false);
        model.addAttribute("user", loggedInUser);

        return "profile";
    }

    /**
     * Changes the activation state for the given username.
     *
     * @param principal        the principal
     * @param request          the request
     * @param response         the response
     * @return the string
     */
    @PostMapping("/deactivate")
    @ResponseBody
    public String deactivateUser(@RequestParam("username") String username,
                             Principal principal, HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {

        // Get the userdata of the user that is requesting the update
        User requestUser = userService.getUserByUserName(principal.getName(), false);

        User userToBeAltered = userService.getUserByUserName(username, false);
        if (requestUser.getRole().equals("ROLE_ADMIN") ) {
            userService.updateAccountActivation(userToBeAltered, false, false);
        } else {
            response.setStatus(401);
            return "{\"status\": false, \"message\": \"Unauthorized\" }";
        }

        return "{\"status\": true, \"message\": \"User "+ username +" activation state changed to false\" }";

    }

    /**
     * Changes the activation state for the given username.
     *
     * @param principal        the principal
     * @param request          the request
     * @param response         the response
     * @return the string
     */
    @PostMapping("/activate")
    @ResponseBody
    public String activateUser(@RequestParam("username") String username,
                                 Principal principal, HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {

        // Get the userdata of the user that is requesting the update
        User requestUser = userService.getUserByUserName(principal.getName(), false);

        User userToBeAltered = userService.getUserByUserName(username, false);
        if (requestUser.getRole().equals("ROLE_ADMIN") ) {
            userService.updateAccountActivation(userToBeAltered, true, false);
        } else {
            response.setStatus(401);
            return "{\"status\": false, \"message\": \"Unauthorized\" }";
        }

        return "{\"status\": true, \"message\": \"User "+ username +" activation state changed to true\" }";

    }

    /**
     * Changes the activation state for the given username.
     *
     * @param principal        the principal
     * @param request          the request
     * @param response         the response
     * @return the string
     */
    @PostMapping("/delete")
    @ResponseBody
    public String deleteUser(@RequestParam("username") String username,
                                 Principal principal, HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {

        // Get the userdata of the user that is requesting the update
        User requestUser = userService.getUserByUserName(principal.getName(), false);

        User userToBeAltered = userService.getUserByUserName(username, false);
        if (requestUser.getRole().equals("ROLE_ADMIN") ) {
            userService.deleteUserLATable(username, false);
            userService.deleteUserRoleTable(username, false);
            userService.deleteUserUserTable(username, false);
        } else {
            response.setStatus(401);
            return "{\"status\": false, \"message\": \"Unauthorized\" }";
        }

        return "{\"status\": true, \"message\": \"User "+ username +" has been deleted\" }";

    }

    /**
     * Updates information of for the given username.
     *
     * @param principal        the principal
     * @param request          the request
     * @param response         the response
     * @return the string
     */
    @PostMapping("/update")
    @ResponseBody
    public String updateUser(@RequestParam("username") String username,
                             @RequestParam("fname") String fname,
                             @RequestParam("lname") String lname,
                             @RequestParam("email") String email,
                             @RequestParam("language") String language,
                             @RequestParam("institute") String institute,
                             @RequestParam("role") String role,
                             @RequestParam("picture") String picture,
                             Principal principal, HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {

        // Get the userdata of the user that is requesting the update
        User requestUser = userService.getUserByUserName(principal.getName(), false);

        User userToBeAltered = userService.getUserByUserName(username, false);
        if (requestUser.getUserName().equals(username) || requestUser.getRole().equals("ROLE_ADMIN") ) {
            // User is authorized to make changes to the user requested


            // Create a new empty user object
            UserNew userToUpdate = new UserNew();

            // Fill the user with the submitted values
            userToUpdate.setUserName(username);
            userToUpdate.setFirst_name(fname);
            userToUpdate.setLast_name(lname);
            userToUpdate.setEmail(email);
            userToUpdate.setProfile_language(language);
            userToUpdate.setPassword_reset_link_valid(0);

            // Set original values to role specific fields
            if (requestUser.getRole().equals("ROLE_ADMIN")) {
                userToUpdate.setInstitute(institute);
                userToUpdate.setRole(role);
                userToUpdate.setProfile_picture(userToBeAltered.getProfile_picture());
            } else {
                userToUpdate.setInstitute(userToBeAltered.getInstitute());
                userToUpdate.setRole(userToBeAltered.getRole());
                userToUpdate.setProfile_picture(picture);
            }


            // Update the user in the database
            userService.updateUser(userToUpdate, false);
            userService.updateRole(userToUpdate,false);

        } else {
            logger.log(Level.WARNING, "User "+ requestUser.getUserName() + " tried to update details of user "+ username + " from IP "+ request.getRemoteAddr());
            return "User is not authorized to do this";
        }

        return "Updated";

    }

    @GetMapping("/new")
    public String createUser(Model model, Principal principal) {

        // Get the current logged in user
        User user = userService.getUserByUserName(principal.getName(), false);
        // Get a list of all the institutes
        List<Institute> instituteList = instituteService.getInstitutes(false);

        // Add the models
        model.addAttribute("user", user);
        model.addAttribute("institutes", instituteList);
        model.addAttribute("newUser", new User());
        return "create-user";
    }

    @PostMapping("/new/save")
    public String saveNewUser(@Valid @ModelAttribute("newUser") UserNew newUser, BindingResult bindingResult, Model model, Principal principal) throws IOException, SQLException {

        // Get the current logged in user
        User user = userService.getUserByUserName(principal.getName(), false);

        // Add the models
        model.addAttribute("user", user);
        model.addAttribute("newUser", newUser);

        // If the added user data contains errors
        if (bindingResult.hasErrors()) {
            // Add the list with institutes to the page and show the create page, with the new user data and an error message
            List<Institute> instituteList = instituteService.getInstitutes(false);
            model.addAttribute("institutes", instituteList);
            return "create-user";
        }

        // Generate a 2FA secret and set this to the user model
        String totp_secret = Base32.random();
        newUser.setTotp_secret(totp_secret);

        // Get a number of usernames that are registered with the given username
        int num_of_usersnames = userService.getNumberOfUsernames(newUser.getUserName(), false);

        // Check if the looked up number is higher than zero, this means that the username is already taken
        if (num_of_usersnames > 0 ){

            logger.log(Level.INFO, "Username " + newUser.getUserName() + " already present in table, trying other options");

            int counter = 1;

            // Add a number after the username and check if the username still exists. Increase the number every try
            while (num_of_usersnames > 0) {
                counter++;
                num_of_usersnames = userService.getNumberOfUsernames(newUser.getUserName()+counter, false);
            }

            logger.log(Level.INFO, "Found new username for user " + newUser.getUserName() + ". New username will be: " + newUser.getUserName() + counter);

            // Update the (generated) username in the user model
            newUser.setUserName(newUser.getUserName() + counter);
        }

        // Generate a password "reset" token
        String token = Base32.random();

        // Set the password "reset" token
        newUser.setPassword_token(token);

        // Set the password link valid
        newUser.setPassword_reset_link_valid(0);

        // Insert the user to the database
        userService.insertUser(newUser, false);
        userService.insertRole(newUser, false);
        userService.addLoginAttempts(newUser, false);

        // Construct a "reset" password URL
        String reactURL = "http://"+appHost+"/user/activate?username="+newUser.getUserName()+"&token="+token;

        String content;
        String subject;

        // Set up the mail data to match the user's locale
        if (newUser.getProfile_language().equals("nl-NL")) {
            content = "Hi "+newUser.getFirst_name()+",<br>" +
                    mail_new_account_nl+"<br>" +
                    mail_new_account_username_nl + " <b>" + newUser.getUserName() + "</b><br>" +
                    mail_activate_nl;
            subject = mail_new_account_subject_nl;
        } else {
            content = "Hi "+newUser.getFirst_name()+",<br>" +
                    mail_new_account_en + "<br>" +
                    mail_new_account_username_en + " <b>" + newUser.getUserName() + "</b><br>" +
                    mail_activate_en;
            subject = mail_new_account_subject_en;
        }

        String emailGeneral = "<br><br>" +
                "<a href=\""+reactURL+"\">"+reactURL+"</a>" + mail_general_footer;

        content += emailGeneral;

        // Send the email in the background (so the user won't get a timeout, beacuse this can take some time)
        ExecutorService service = Executors.newFixedThreadPool(4);
        String finalContent = content;
        service.submit(new Runnable() {
            public void run() {
                try {
                    emailService.sendmail(subject, finalContent, "contentTYpe", newUser.getEmail());
                } catch (MessagingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        logger.log(Level.INFO, "User "+user.getUserName() + "Added a new user with the username "+ newUser.getUserName());

        return "saved-user";
    }

    /**
     * This method returns the user activation page.
     *
     * @param username         the username
     * @param token            the token
     * @param model            the model
     * @param principal        the principal
     * @param request          the request
     * @param response         the response
     * @return the string
     */
    @RequestMapping("/activate")
    public String showActivationPage(
            @RequestParam(value = "username", required = false, defaultValue = "") String username,
            @RequestParam(value = "token", required=false, defaultValue="") String token,
            Model model, Principal principal, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {

        localeService.setLocale(principal, response, request);

        // Check if none of the parameters is missing. If so, show the invalid activation page
        if (username == null || username.equals("") || token == null || token.equals("")) {
            return "invalid_activation";
        }

        User user = null;

        // Look up the user, if it does not exist, show the invalid activation page
        try {
            user = userService.getUserDataForLogin(username, false);
        } catch (Exception e) {
            return "invalid_activation";
        }

        // Check if the user is already activated. If so, show the login page
        if (user.getEnabled() > 0) {
            // User is already activated, redirect to login
            return "redirect:/login";
        }

        // Check if the submitted token corresponds to the user saved token. Otherwise, show the invalid activation page
        if (!token.equals(user.getPassword_token())) {
            return "invalid_activation";
        }

        // Add the userdata and 2FA token to the response and show the activated page
        model.addAttribute("username", username);
        model.addAttribute("token", token);
        return "activate";
    }

    /**
     * This method activate the user via REST.
     *
     * @param username         the username
     * @param token            the token
     * @param model            the model
     * @param principal        the principal
     * @param request          the request
     * @param response         the response
     * @return the string
     */
    @PostMapping("/activate/save")
    public String showActivationPage(
            @RequestParam(value = "username", required = false, defaultValue = "") String username,
            @RequestParam(value = "token", required=false, defaultValue="") String token,
            @RequestParam(value = "password1", required=false, defaultValue="") String password1,
            @RequestParam(value = "password2", required=false, defaultValue="") String password2,
            Model model, Principal principal, HttpServletRequest request, HttpServletResponse response) {

        localeService.setLocale(principal, response, request);

        // Check if none of the parameters is missing. If so, show the invalid activation page
        if (username == null || username.isEmpty() ||
                token == null || token.isEmpty() ||
                password1 == null || password1.isEmpty() ||
                password2 == null || password2.isEmpty()) {
            return "invalid_activation";
        }

        User user = null;

        // Look up the user, if it does not exist, show the invalid activation page
        try {
            user = userService.getUserDataForLogin(username, false);
        } catch (Exception e) {
            return "invalid_activation";
        }

        // Check if the user is already activated. If so, show the login page
        if (user.getEnabled() > 0) {
            // User is already activated, redirect to login
            return "redirect:/login";
        }
        // Check if the submitted token corresponds to the user saved token. Otherwise, show the invalid activation page
        if (!token.equals(user.getPassword_token())) {
            logger.log(Level.WARNING, "Account of user "+username+" could not be activated, token is not valid");
            return "invalid_activation";
        }

        // Set up an encrypter for the password
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(12);

        // Check if the two submitted passwords are equal, if not redirect to the original activation page with an error parameter
        if (!password1.equals(password2)) {
            logger.log(Level.WARNING, "Account of user "+username+" could not be activated, passwords do not match");
            return "redirect:/user/activate?username="+username+"&token="+token+"&passwordsdontmatch=true";
        }

        // Check if the submitted password is equal to the password already in use, if not redirect to the original activation page with an error parameter
        if (passwordEncoder.matches(password1, user.getPassword())) {
            logger.log(Level.WARNING, "Account of user "+username+" could not be activated, new password matches the old one");
            return "redirect:/user/activate?username="+username+"&token="+token+"&passwordmatchesoldone=true";
        }

        // Set the values in the user model
        user.setUserName(username);
        user.setPassword(passwordEncoder.encode(password1));

        // Update the database
        //TODO: Eventually these 3 SQL functions can be combined in to one with just one model
        try {
            userService.updatePassword(user, false);
        }  catch (IOException e) {
            logger.log(Level.WARNING, "Account of user "+username+" could not be activated, IOException while updating password in DB");
            e.printStackTrace();
            return "redirect:/user/activate?username="+username+"&token="+token+"&failed=true";
        } catch (SQLException e) {
            logger.log(Level.WARNING, "Account of user "+username+" could not be activated, SQLException while updating password in DB");
            e.printStackTrace();
            return "redirect:/user/activate?username="+username+"&token="+token+"&failed=true";
        }


        try {
            userService.updateAccountActivation(user, true, false);
        } catch (IOException e) {
            logger.log(Level.WARNING, "Account of user "+username+" could not be activated, IOException while updating DB");
            e.printStackTrace();
            return "redirect:/user/activate?username="+username+"&token="+token+"&failed=true";
        } catch (SQLException e) {
            logger.log(Level.WARNING, "Account of user "+username+" could not be activated, SQLException while updating DB");
            e.printStackTrace();
            return "redirect:/user/activate?username="+username+"&token="+token+"&failed=true";
        }

        try {
            userService.updatePasswordToken(user, null, false);
        } catch (IOException e) {
            logger.log(Level.WARNING, "Account of user "+username+" could not be activated, IOException while updating token in DB");
            e.printStackTrace();
            return "redirect:/user/activate?username="+username+"&token="+token+"&failed=true";
        } catch (SQLException e) {
            logger.log(Level.WARNING, "Account of user "+username+" could not be activated, SQLException while updating token in DB");
            e.printStackTrace();
            return "redirect:/user/activate?username="+username+"&token="+token+"&failed=true";
        }

        // Remove the password token from the model
        user.setPassword_token(null);

        // Add the model to the response and show the activated page with instructions to set up 2FA
        model.addAttribute("user", user);
        return "activate2fa";
    }






    /**
     * This method returns the user activation page.
     *
     * @param username         the username
     * @param model            the model
     * @param principal        the principal
     * @param request          the request
     * @param response         the response
     * @return the string
     */
    @RequestMapping("/password/request-reset")
    public String requestPasswordReset(
            @RequestParam(value = "username", required = false, defaultValue = "") String username,
            @RequestParam(value = "email", required = false, defaultValue = "") String email,
            Model model, Principal principal, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {

        localeService.setLocale(principal, response, request);

        // Check if none of the parameters is missing. If so, show the invalid activation page
        if (username == null || username.equals("") ||
                email == null || email.equals("")) {
            logger.log(Level.WARNING, "Could not reset password for user "+username+", empty values");
            return "invalid_reset";
        }

        User user = null;

        // Look up the user, if it does not exist, show the invalid activation page
        try {
            user = userService.getUserDataForLogin(username, false);
        } catch (Exception e) {
            logger.log(Level.WARNING, "Could not reset password for user "+username+", user does not exist.");
            return "invalid_reset";
        }

        // Verify the parameters provided by the user
        if (!email.toLowerCase().equals(user.getEmail().toLowerCase())) {
            logger.log(Level.WARNING, "Could not reset password for user "+user.getUserName()+", email is not the same.");
            return "invalid_reset";
        }

        // check if user should reactivate instead of activate
        if (user.getPassword() == null || user.getPassword().equals("") || user.getEnabled() < 1) {
            logger.log(Level.WARNING, "Could not reset password for user "+user.getUserName()+", account is not activated.");
            return "invalid_reset";
        }

        User userFullDetails = null;
        // Look up the user, if it does not exist, show the invalid activation page
        try {
            userFullDetails = userService.getUserByUserName(username, false);
        } catch (Exception e) {
            logger.log(Level.WARNING, "Could not reset password for user "+user.getUserName()+", account full details not found.");
            return "invalid_reset";
        }

        /*
        *  EVERYTHING IS ALL RIGHT
        */

        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());

        String datetime = timestamp.toString();

        try {
            userService.updatePasswordReset(user.getUserName(), datetime, false);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        String base32Token = Base32.random();

        byte[] encoded = base32Token.getBytes();

        // create new key
        SecretKey secretKey = null;
        try {
            secretKey = KeyGenerator.getInstance("AES").generateKey();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
// get base64 encoded version of the key
        String encodedKey = Base64.getEncoder().encodeToString(secretKey.getEncoded());

        try {
            userService.updatePasswordToken(user, encodedKey, false);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // Everything is okay. Make the link invalid
        try {
            userService.updatePasswordLinkValid(user.getUserName(), true, false);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        User jwtUser = new User();

        jwtUser.setEmail(userFullDetails.getEmail());
        jwtUser.setUserName(userFullDetails.getUserName());

        // SIGN A JWT TOKEN
        String jwtToken = jwtTokenGenerator.generateToken(null , jwtUser, "600000", encodedKey);


        sendPasswordResetEmail(userFullDetails, jwtToken);

        return "password-request_submitted";
    }

    private void sendPasswordResetEmail(User user, String token) {

        String reactURL = "http://"+appHost+"/user/password/reset?username="+user.getUserName()+"&token="+token;

        String content;
        String subject;

        if (user.getProfile_language().equals("nl-NL")) {
            content = "Hi "+user.getFirst_name()+",<br>" +
                    mail_reset_body_nl+"<br>" +
                    mail_reset_not_you_nl;
            subject = mail_reset_subject_nl;
        } else {
            content = "Hi "+user.getFirst_name()+",<br>" +
                    mail_reset_body_en+"<br>" +
                    mail_reset_not_you_en;
            subject = mail_reset_subject_en;
        }

        String emailGeneral = "<br><br>" +
                "<a href=\""+reactURL+"\">"+reactURL+"</a>" + mail_general_footer;

        content += emailGeneral;

        ExecutorService service = Executors.newFixedThreadPool(4);
        String finalContent = content;
        service.submit(new Runnable() {
            public void run() {
                try {
                    emailService.sendmail(subject, finalContent, "contentTYpe", user.getEmail());
                } catch (MessagingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    /**
     * This method returns the user reset password page.
     *
     * @param username         the username
     * @param token            the token
     * @param model            the model
     * @param principal        the principal
     * @param request          the request
     * @param response         the response
     * @return the string
     */
    @RequestMapping("/password/reset")
    public String showReactivationPage(
            @RequestParam(value = "username", required = false, defaultValue = "") String username,
            @RequestParam(value = "token", required=false, defaultValue="") String token,
            Model model, Principal principal, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {

        localeService.setLocale(principal, response, request);

        // Check if none of the parameters is missing. If so, show the invalid activation page
        if (username == null || username.equals("") || token == null || token.equals("")) {
            logger.log(Level.WARNING, "Could not reset password for user "+username+", empty params.");
            return "invalid_reset";
        }

        User user = null;

        // Look up the user, if it does not exist, show the invalid activation page
        try {
            user = userService.getUserDataForLogin(username, false);
        } catch (Exception e) {
            logger.log(Level.WARNING, "Could not reset password for user "+username+", account is not found.");
            return "invalid_reset";
        }

        // Check if the user is already activated. If not, make the reset invalid
        if (user.getEnabled() < 1) {
            // User is not activated, redirect to login
            logger.log(Level.WARNING, "Could not reset password for user "+user.getUserName()+", account is not activated.");
            return "invalid_reset";
        }

        // Check if the user is already activated. If not, make the reset invalid
        if (user.getPassword_reset_link_valid() < 1) {
            // Link is not valid anymore
            logger.log(Level.WARNING, "Could not reset password for user "+user.getUserName()+", URL is not valid.");
            return "invalid_reset";
        }

        // check if user should reactivate instead of activate
        if (user.getPassword() == null || user.getPassword().equals("")) {
            redirectAttributes.addAttribute("username", username);
            redirectAttributes.addAttribute("token", token);
            logger.log(Level.WARNING, "Could not reset password for user "+user.getUserName()+", user should activate.");
            return "activate";
        }

        String jwtUser = null;

        // Check if the submitted token corresponds to the user saved token. Otherwise, show the invalid activation page
        try {
            jwtUser = JWT.require(Algorithm.HMAC512(user.getPassword_token()))
                    .build()
                    .verify(token)
                    .getSubject();
        } catch (TokenExpiredException tokenExpiredException) {
            logger.log(Level.WARNING, "Token expired for user " + user.getUserName());
            return "invalid_reset";
        } catch (Exception e) {
            logger.log(Level.WARNING, "Token exception " + user.getUserName());
            return "invalid_reset";
        }

        if (jwtUser == null) {
            logger.log(Level.WARNING, "Could not reset password for user "+user.getUserName()+", JWT user equals null.");
            return "invalid_reset";
        }

        if (!jwtUser.equals(user.getUserName())) {
            logger.log(Level.WARNING, "Could not reset password for user "+user.getUserName()+", username provides does not corresponds to username from token.");
            return "invalid_reset";
        }

        // Everything is okay. Make the link invalid
        try {
            userService.updatePasswordLinkValid(user.getUserName(), false, false);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        // Add the userdata and 2FA token to the response and show the activated page
        model.addAttribute("username", username);
        model.addAttribute("token", token);
        return "reset_password";
    }




    /**
     * This method reactivates the user via REST.
     *
     * @param username         the username
     * @param token            the token
     * @param model            the model
     * @param principal        the principal
     * @param request          the request
     * @param response         the response
     * @return the string
     */
    @PostMapping("/password/reset/save")
    public String saveReactivation(
            @RequestParam(value = "username", required = false, defaultValue = "") String username,
            @RequestParam(value = "token", required=false, defaultValue="") String token,
            @RequestParam(value = "password1", required=false, defaultValue="") String password1,
            @RequestParam(value = "password2", required=false, defaultValue="") String password2,
            @RequestParam(value = "code", required=false, defaultValue="") String totp,
            Model model, Principal principal, HttpServletRequest request, HttpServletResponse response) {

        localeService.setLocale(principal, response, request);

        // Check if none of the parameters is missing. If so, show the invalid activation page
        if (username == null || username.isEmpty() ||
                token == null || token.isEmpty() ||
                password1 == null || password1.isEmpty() ||
                password2 == null || password2.isEmpty() ||
                totp == null || totp.isEmpty()) {
            return "invalid_reset";
        }

        User user = null;

        // Look up the user, if it does not exist, show the invalid activation page
        try {
            user = userService.getUserDataForLogin(username, false);
        } catch (Exception e) {
            return "invalid_reset";
        }

        String jwtUser = null;
        // Check if the submitted token corresponds to the user saved token. Otherwise, show the invalid activation page
        try {
            jwtUser = JWT.require(Algorithm.HMAC512(user.getPassword_token()))
                    .build()
                    .verify(token)
                    .getSubject();
        } catch (TokenExpiredException tokenExpiredException) {
            logger.log(Level.WARNING, "Token expired for user " + user.getUserName());
            return "invalid_reset";
        } catch (Exception e) {
            logger.log(Level.WARNING, "Token exception " + user.getUserName());
            return "invalid_reset";
        }

        if (jwtUser == null) {
            logger.log(Level.WARNING, "Could not reset password for user "+user.getUserName()+", JWT user equals null.");
            return "invalid_reset";
        }

        if (!jwtUser.equals(user.getUserName())) {
            logger.log(Level.WARNING, "Could not reset password for user "+user.getUserName()+", username provides does not corresponds to username from token.");
            return "invalid_reset";
        }

        // Set up an encrypter for the password
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(12);

        // Check if the two submitted passwords are equal, if not redirect to the original activation page with an error parameter
        if (!password1.equals(password2)) {
            logger.log(Level.WARNING, "Password of user "+username+" could not be reset, passwords do not match");
            return "redirect:/user/password/reset?username="+username+"&token="+token+"&passwordsdontmatch=true";
        }

        // Check if the submitted password is equal to the password already in use, if not redirect to the original activation page with an error parameter
        if (passwordEncoder.matches(password1, user.getPassword())) {
            logger.log(Level.WARNING, "Password of user "+username+" could not be reset, new password matches the old one");
            return "redirect:/user/password/reset?username="+username+"&token="+token+"&passwordmatchesoldone=true";
        }

        // Verify topt secret
        Totp totpPwdReset = new Totp(user.getTotp_secret());

        // Check if the code is a valid type long and if the code is valid, if not throw an badcredentials exception
        if (!isValidLong(totp) || !totpPwdReset.verify(totp)) {
            // TOTP is invalid
            logger.log(Level.WARNING, "Password of user "+user.getUserName()+" could not be reset, totp is invalid");
            return "invalid_reset";
        }

        System.out.println("USER PASSWORD WILL BE RESETTED");

        // Set the values in the user model
        user.setUserName(username);
        user.setPassword(passwordEncoder.encode(password1));

        // Update the database
        //TODO: Eventually these 3 SQL functions can be combined in to one with just one model
        try {
            userService.updatePassword(user, false);
        }  catch (IOException e) {
            logger.log(Level.WARNING, "Account of user "+username+" could not be activated, IOException while updating password in DB");
            e.printStackTrace();
            return "redirect:/user/password/reset?username="+username+"&token="+token+"&failed=true";
        } catch (SQLException e) {
            logger.log(Level.WARNING, "Account of user "+username+" could not be activated, SQLException while updating password in DB");
            e.printStackTrace();
            return "redirect:/user/password/reset?username="+username+"&token="+token+"&failed=true";
        }

        try {
            userService.updatePasswordToken(user, null, false);
        } catch (IOException e) {
            logger.log(Level.WARNING, "Account of user "+username+" could not be activated, IOException while updating token in DB");
            e.printStackTrace();
            return "redirect:/user/password/reset?username="+username+"&token="+token+"&failed=true";
        } catch (SQLException e) {
            logger.log(Level.WARNING, "Account of user "+username+" could not be activated, SQLException while updating token in DB");
            e.printStackTrace();
            return "redirect:/user/password/reset?username="+username+"&token="+token+"&failed=true";
        }

        // Everything is okay. Make the link invalid
        try {
            userService.updatePasswordLinkValid(user.getUserName(), false, false);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        LoginAttempts loginAttempts = userService.getLoginAttemptsByUsername(username, false);

        loginAttempts.setNumber_of_wrong_attempts(0);

        try {
            userService.updateLoginAttempts(loginAttempts, false);
        } catch (IOException e) {
            logger.log(Level.WARNING, "Account of user "+username+" could not be activated, IOException while updating DB");
            e.printStackTrace();
            return "redirect:/user/password/reset?username="+username+"&token="+token+"&failed=true";
        } catch (SQLException e) {
            logger.log(Level.WARNING, "Account of user "+username+" could not be activated, SQLException while updating DB");
            e.printStackTrace();
            return "redirect:/user/password/reset?username="+username+"&token="+token+"&failed=true";
        }

        // Remove the password token from the model
        user.setPassword_token(null);

        // Add the model to the response and show the activated page with instructions to set up 2FA
        model.addAttribute("user", user);
        return "password-resetted";
    }

    private boolean isValidLong(String code) {
        try {
            Long.parseLong(code);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}

