package nl.bioinf.spraak.controllers;

/**
 *  created by Marcel Zandberg
 */
import nl.bioinf.spraak.models.Institute;
import nl.bioinf.spraak.models.User;
import nl.bioinf.spraak.service.InstituteService;
import nl.bioinf.spraak.service.LocaleService;
import nl.bioinf.spraak.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Settings controller serves the settings page to configure spraak.
 */
@Controller
public class SettingsController {

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    /**
     * The User service.
     */
    @Autowired
    UserService userService;

    /**
     * The Locale service.
     */
    @Autowired
    LocaleService localeService;

    /**
     * The Institute service.
     */
    @Autowired
    InstituteService instituteService;

    /**
     * Greeting form string.
     *
     * @param model     the model
     * @param principal the principal
     * @param username  the username
     * @param request   the request
     * @param response  the response
     * @return the string
     */
    @GetMapping("/settings")
    public String greetingForm(Model model, Principal principal, String username, HttpServletRequest request, HttpServletResponse response){
        username = principal.getName();
        User userData = userService.getUserByUserName(username, false);

        localeService.setLocale(principal, response, request);

        model.addAttribute("institute", new Institute());
        model.addAttribute("user", userData);

        logger.log(Level.INFO,"showing settings page");

        return "settings";
    }


    /**
     * Registration string.
     *
     * @param instituteForm the institute form
     * @param bindingResult the binding result
     * @param model         the model
     * @param principal     the principal
     * @param request       the request
     * @param response      the response
     * @return the string
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    @PostMapping("/institute")
    public String registration(@ModelAttribute("institute") @Valid Institute instituteForm, BindingResult bindingResult, Model model, Principal principal, HttpServletRequest request, HttpServletResponse response)  throws IOException, SQLException {
        String username = principal.getName();
        User userData = userService.getUserByUserName(username, false);

        localeService.setLocale(principal, response, request);

        if (bindingResult.hasErrors()) {
            model.addAttribute("user", userData);
            System.out.println("error");
            return "settings";
        }

        //inserts a new institute in the db
        try{
            instituteService.insertInstitute(instituteForm, false);
        } catch (DataAccessException dae){
            logger.log(Level.WARNING, "you did not register the user in the db" + dae.getCause().getLocalizedMessage());
            model.addAttribute("user", userData);
            model.addAttribute("error","This institute already exists");
            return "settings";
        }
        logger.log(Level.WARNING, "you registered the institute in the db as host:"  + instituteForm.getApi_host());
        model.addAttribute("user", userData);
        return "registered_institute";
    }

}
