package nl.bioinf.spraak.controllers;

/**
 *  created by Bas Kasemir
 */

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Wireframe controller sets the layout of the app.
 */
@Controller
public class WireframeController {

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    /**
     * Greeting string.
     *
     * @param code  the code
     * @param model the model
     * @return the string
     */
    @GetMapping("/wireframe")
    public String greeting(@RequestParam(name="code", required=false, defaultValue="0") String code, Model model) {


        logger.log(Level.INFO, "wireframe loaded");
        model.addAttribute("code", code);
        return "fragments/wireframe";
    }

}