package nl.bioinf.spraak.controllers;

/**
 *  Copyright 2019 Bas Kasemir
 */

import nl.bioinf.spraak.models.User;
import nl.bioinf.spraak.service.LocaleService;
import nl.bioinf.spraak.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Results controller serves the results page and gets the results.
 */
@Controller
@RequestMapping("/patients")
public class PatientController {

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    /**
     * The Locale service.
     */
    @Autowired
    LocaleService localeService;

    private final UserService userService;

    /**
     * Instantiates a new Results controller.
     *
     * @param userService the user service
     */
    @Autowired
    public PatientController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Show results string.
     *
     * @param username  the username
     * @param model     the model
     * @param principal the principal
     * @param request   the request
     * @param response  the response
     * @return the string
     */
    @GetMapping
    public String showResults(String username, Model model, Principal principal, HttpServletRequest request, HttpServletResponse response) {
        username = principal.getName();
        User userData = userService.getUserByUserName(username, false);

        model.addAttribute("user", userData);

        localeService.setLocale(principal, response, request);

        logger.log(Level.INFO,"showing patient page");

        return "patient";
    }


}
