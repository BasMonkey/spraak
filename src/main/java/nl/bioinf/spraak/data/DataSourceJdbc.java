package nl.bioinf.spraak.data;

/**
 *  created by Marcel Zandberg, extended by Bas Kasemir and Marcel Zandberg
 *  Extended during internship by Bas Kasemir
 */

import nl.bioinf.spraak.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;
import java.sql.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Data source jdbc connects with db, insert and retrieve.
 */
@Component
public class DataSourceJdbc implements DataSource {

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedJdbcTemplate;
    /**
     * The Insert user query.
     */
    final String INSERT_USER_QUERY = "insert into Spraak_users (`first_name`, `last_name`, `email`, `userName`, `totp_secret`, `password_token`, `profile_language`, `profile_picture`, `institute`, `enabled`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    /**
     * The Add to login attepmts query
     */
    final String INSERT_LOGIN_ATTEMPTS = "insert into Spraak_users_login_attempts (`username`, `number_of_wrong_attempts`) VALUES (?, ?)";

    /**
     * The count exsisting usernames Query
     */
    final String COUNT_USERNAMES_QUERY = "SELECT COUNT(*) FROM Spraak_users WHERE userName = ?";

    /**
     * The Disabel user query.
     */
    final String UPDATE_USER_ACTIVATION_UERY = "update Spraak_users set enabled = ? where username = ?";

    /**
     * The DELETE from user table query.
     */
    final String DELETE_USERTABLE_QUERY = "DELETE FROM Spraak_users WHERE username = ?";

    /**
     * The DELETE from Spraak_users_roles table query.
     */
    final String DELETE_ROLETABLE_QUERY = "DELETE FROM Spraak_users_roles WHERE username = ?";

    /**
     * The DELETE from Spraak_users_login_attempts table query.
     */
    final String DELETE_LATABLE_QUERY = "DELETE FROM Spraak_users_login_attempts WHERE username = ?";

    /**
     *  The UPDATE_PASSWORD_LINK_VALID query.
     */
    final String UPDATE_PASSWORD_LINK_VALID = "update Spraak_users set password_reset_link_valid = ? where username = ?";

    /**
     *  The UPDATE_PASSWORD_RESET query.
     */
    final String UPDATE_PASSWORD_RESET = "update Spraak_users set password_timestamp = ? where username = ?";

    /**
     *  The UPDATE_PASSWORD_TOKEN query.
     */
    final String UPDATE_PASSWORD_TOKEN = "update Spraak_users set password_token = ? where username = ?";

    /**
     *  The UPDATE_PASSWORD_TOKEN query.
     */
    final String UPDATE_PASSWORD = "update Spraak_users set password = ? where username = ?";

    /**
     * The Insert praat query.
     */
    final String INSERT_PRAAT_QUERY = "insert into Spraak_PraatResults (`patient_number`,`userName`, `sound_file`, `nsyll`, `npause`, `dur_in_sec`, `phonation_time`, `speech_rate`, `articulation`, `ASD`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    /**
     * The Insert soundfile query.
     */
    final String INSERT_SOUNDFILE_QUERY = "insert into Spraak_soundfile (`username`, `sound_file`, `sound_data`) values (?,?,?)";
    /**
     * The Insert opensmile query.
     */
    final String INSERT_OPENSMILE_QUERY = "insert into spraak_opensmileresults (`username`, `patient_number`, `openSmile_file`) values (?,?,?)";
    /**
     * The Insert institute query.
     */
    final String INSERT_INSTITUTE_QUERY = "insert into Spraak_institutes (`abbreviation`, `full_name`, api_host) values (?, ?, ?)";
    /**
     * The Update user query.
     */
    final String UPDATE_USER_QUERY = "update Spraak_users set first_name = ?, last_name = ?, email = ?, userName = ?, profile_language = ?, profile_picture = ?, institute = ? WHERE userName = ?";
    /**
     * The Insert role query.
     */
    final String INSERT_ROLE_QUERY = "insert into Spraak_users_roles (`username`, `role`) values (?, ?)";
    /**
     * The Update role query.
     */
    final String UPDATE_ROLE_QUERY = "update Spraak_users_roles set username = ?, role = ? where username = ?";

    final String UPDATE_LOGIN_ATTEMPT_QUERY = "update Spraak_users_login_attempts set number_of_wrong_attempts = ? where username = ?";


    /**
     * Instantiates a new Data source jdbc.
     *
     * @param jdbcTemplate      the jdbc template
     * @param namedJdbcTemplate the named jdbc template
     */
    @Autowired
    public DataSourceJdbc(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedJdbcTemplate = namedJdbcTemplate;
    }

    @Override
    public Institute getInstituteByAbbreviation(String abbreviation) {
        logger.log(Level.INFO, "Getting Institute info from institute table");
        return namedJdbcTemplate.queryForObject("SELECT abbreviation, full_name, api_host FROM Spraak_institutes WHERE abbreviation =:abbr",
                new MapSqlParameterSource("abbr", abbreviation),
                BeanPropertyRowMapper.newInstance(Institute.class));
    }

    @Override
    public User getUserByUserName(String userName) { //select Spraak_users.userName, role from Spraak_users join Spraak_users_roles on Spraak_users_roles.username = Spraak_users.userName;
        logger.log(Level.INFO, "Getting user info from user table");
        return namedJdbcTemplate.queryForObject("SELECT  id, first_name, last_name, email, Spraak_users.userName, Spraak_users.institute as institute_abbr, Spraak_institutes.full_name as full_name_institute, created_date, profile_picture, profile_language, enabled, role FROM Spraak_users join Spraak_users_roles on Spraak_users_roles.username = Spraak_users.username join Spraak_institutes on Spraak_institutes.abbreviation = Spraak_users.institute WHERE Spraak_users.username =:abbr",
                new MapSqlParameterSource("abbr", userName),
                BeanPropertyRowMapper.newInstance(User.class));
    }

    @Override
    public User getUserDataForLogin(String username) {
        logger.log(Level.INFO, "Getting user login info from user table");
        return namedJdbcTemplate.queryForObject("SELECT  id, Spraak_users.userName, email, password, totp_secret, created_date, password_token, password_timestamp, password_reset_link_valid, profile_language, enabled, role FROM Spraak_users join Spraak_users_roles on Spraak_users_roles.username = Spraak_users.username WHERE Spraak_users.username =:abbr",
                new MapSqlParameterSource("abbr", username),
                BeanPropertyRowMapper.newInstance(User.class));
    }

    @Override
    public LoginAttempts getLoginAttemptsByUsername(String username) { //select Spraak_users.userName, role from Spraak_users join Spraak_users_roles on Spraak_users_roles.username = Spraak_users.userName;
        logger.log(Level.INFO, "getting user login attempts");
        return namedJdbcTemplate.queryForObject("SELECT * FROM Spraak_users_login_attempts WHERE username =:abbr",
                new MapSqlParameterSource("abbr", username),
                BeanPropertyRowMapper.newInstance(LoginAttempts.class));
    }

    @Override
    public int updateLoginAttempts(LoginAttempts loginAttempts) throws IOException, SQLException {
        logger.log(Level.INFO, "Updating user login attempts");
        return jdbcTemplate.update(UPDATE_LOGIN_ATTEMPT_QUERY, loginAttempts.getNumber_of_wrong_attempts(), loginAttempts.getUsername());
    }

    @Override
    public int updateAccountActivation(User user, boolean activated) throws IOException, SQLException {
        return jdbcTemplate.update(UPDATE_USER_ACTIVATION_UERY, activated, user.getUserName());
    }

    @Override
    public int updatePasswordLinkValid(String username, boolean bool) throws IOException, SQLException {
        logger.log(Level.INFO, "Updating password link valid for user "+username);
        return jdbcTemplate.update(UPDATE_PASSWORD_LINK_VALID, bool, username);
    }

    @Override
    public int updatePasswordReset(String username, String datetime) throws IOException, SQLException {
        logger.log(Level.INFO, "Updating password reset for user "+username);
        return jdbcTemplate.update(UPDATE_PASSWORD_RESET, datetime, username);
    }

    @Override
    public int updatePasswordToken(User user, String token) throws IOException, SQLException {
        logger.log(Level.INFO, "Updating password token for user "+user.getUserName());
        return jdbcTemplate.update(UPDATE_PASSWORD_TOKEN, token, user.getUserName());
    }

    @Override
    public int updatePassword(User user) throws IOException, SQLException {
        logger.log(Level.INFO, "Updating password for user "+user.getUserName());
        return jdbcTemplate.update(UPDATE_PASSWORD, user.getPassword(), user.getUserName());
    }

    @Override
    public User getUserByPassword(String password) { //select Spraak_users.userName, role from Spraak_users join Spraak_users_roles on Spraak_users_roles.username = Spraak_users.userName;
        logger.log(Level.INFO, "getting user info by password from user table");
        return namedJdbcTemplate.queryForObject("SELECT  id, first_name, last_name, email, Spraak_users.userName, Spraak_users.institute as institute_abbr, Spraak_institutes.full_name as full_name_institute, created_date, profile_picture, profile_language, role FROM Spraak_users join Spraak_users_roles on Spraak_users_roles.username = Spraak_users.userName join Spraak_institutes on Spraak_institutes.abbreviation = Spraak_users.institute WHERE Spraak_users.password =:abbr",
                new MapSqlParameterSource("abbr", password),
                BeanPropertyRowMapper.newInstance(User.class));
    }


    public List<User> getAllUsers(String username) {
        return namedJdbcTemplate.query("SELECT * FROM Spraak_users",
                BeanPropertyRowMapper.newInstance(User.class));
    }

    public List<Institute> getInstitutes() {
        return namedJdbcTemplate.query("SELECT * FROM Spraak_institutes",
                BeanPropertyRowMapper.newInstance(Institute.class));
    }

    @Override
    public User getUserByEmail(String email) {
        //implementation for password restore
        return null;
    }

    @Override
    public int getNumberOfUsernames(String userName) {

        int result = 0;

        try {
            result = jdbcTemplate.queryForObject(
                    COUNT_USERNAMES_QUERY, new Object[]{userName}, Integer.class);
        } catch (NullPointerException e) {
            result = 0;
        }

        return result;

    }


    @Override
    public int insertSoundFile(Principal principal, SpraakResults spraakResults, MultipartFile file) throws IOException, SQLException {
        byte[] bytes = file.getBytes();
        Blob blob = new javax.sql.rowset.serial.SerialBlob(bytes);
        return jdbcTemplate.update(INSERT_SOUNDFILE_QUERY, principal.getName(), spraakResults.getSoundFile(), blob);
    }

    @Override
    public int insertOpenSmileResults(Principal principal, SpraakResults spraakResults, String input) throws IOException, SQLException {
        byte[] bytes = input.getBytes();
        Blob blob = new javax.sql.rowset.serial.SerialBlob(bytes);
        return jdbcTemplate.update(INSERT_OPENSMILE_QUERY, principal.getName(), spraakResults.getPatientNumber(), blob);
    }

    @Override
    public int insertUser(UserNew registerUser) {
        logger.log(Level.INFO, "inserting user in db");
        return jdbcTemplate.update(INSERT_USER_QUERY, registerUser.getFirst_name(), registerUser.getLast_name(), registerUser.getEmail(), registerUser.getUserName(), registerUser.getTotp_secret(), registerUser.getPassword_token(), registerUser.getProfile_language(), registerUser.getProfile_picture(), registerUser.getInstitute(), 0);
    }

    @Override
    public int deleteUserFromUserTable(String username) {
        logger.log(Level.INFO, "deleting user from user table");
        return jdbcTemplate.update(DELETE_USERTABLE_QUERY, username);
    }

    @Override
    public int deleteUserFromRoleTable(String username) {
        logger.log(Level.INFO, "deleting user from role table");
        return jdbcTemplate.update(DELETE_ROLETABLE_QUERY, username);
    }

    @Override
    public int deleteUserFromLATable(String username) {
        logger.log(Level.INFO, "deleting user from Login attempts");
        return jdbcTemplate.update(DELETE_LATABLE_QUERY, username);
    }

    @Override
    public int addLoginAttempts(UserNew registerUser) {
        logger.log(Level.INFO, "Adding Login attempts to db for user "+ registerUser.getUserName());
        return jdbcTemplate.update(INSERT_LOGIN_ATTEMPTS, registerUser.getUserName(), 0);
    }

    @Override
    public int updateUser(UserNew registerUser) throws IOException, SQLException {
        String userName = registerUser.getUserName();
        logger.log(Level.INFO, "Updating row in db for user "+ registerUser.getUserName());
        return jdbcTemplate.update(UPDATE_USER_QUERY, registerUser.getFirst_name(), registerUser.getLast_name(), registerUser.getEmail(), registerUser.getUserName(), registerUser.getProfile_language(), registerUser.getProfile_picture(), registerUser.getInstitute(), userName);
    }

    @Override
    public int insertInstitute(Institute institute) throws IOException, SQLException {
        return jdbcTemplate.update(INSERT_INSTITUTE_QUERY, institute.getAbbreviation(), institute.getFull_name(), institute.getApi_host());
    }

    @Override
    public int insertPraatMaxUpload() {
        return jdbcTemplate.update("SET GLOBAL max_allowed_packet=104857600");
    }

    @Override
    public int insertPraatResults(SpraakResults spraakResults, Principal principal) throws IOException, SQLException {
//        byte[] bytes = file.getBytes();
//        Blob blob = new javax.sql.rowset.serial.SerialBlob(bytes);
        logger.log(Level.INFO, "inserting praat results in table");
        return jdbcTemplate.update(INSERT_PRAAT_QUERY, spraakResults.getPatientNumber() ,principal.getName(),
                spraakResults.getSoundFile() , spraakResults.getNsyll(), spraakResults.getNpause(), spraakResults.getDuration(), spraakResults.getPhonationTime(), spraakResults.getSpeechRate(), spraakResults.getArticulation(), spraakResults.getASD());
    }

    @Override
    public int insertRole(UserNew registerUser) throws IOException, SQLException {
        return jdbcTemplate.update(INSERT_ROLE_QUERY, registerUser.getUserName(), registerUser.getRole());
    }

    @Override
    public int updateRole(UserNew registerUser) throws IOException, SQLException {
        String username = registerUser.getUserName();
        return jdbcTemplate.update(UPDATE_ROLE_QUERY, registerUser.getUserName(), registerUser.getRole(), username);
    }

    /**
     * Praat results row mapper to map over the lists.
     */
    static class PraatResultsRowMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return null;
        }
    }

}

