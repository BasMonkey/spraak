package nl.bioinf.spraak.service;

/**
 *  created by Bas Kasemir
 */

import nl.bioinf.spraak.data.DataSource;
import nl.bioinf.spraak.models.Institute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Institute service.
 */
@Service
public class InstituteService {

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    private final DataSource dataSource;

    /**
     * Instantiates a new Institute service.
     *
     * @param dataSource the data source
     */
    @Autowired
    public InstituteService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Gets institute by abbreviation.
     *
     * @param abbreviation    the abbreviation
     * @param usePartMatching the use part matching
     * @return the institute by abbreviation
     */
    public Institute getInstituteByAbbreviation(String abbreviation, boolean usePartMatching) {
        if (!usePartMatching) {
            logger.log(Level.INFO, "Institute get data by abbreviation executed");
            return dataSource.getInstituteByAbbreviation(abbreviation);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    /**
     * Gets institutes.
     *
     * @param usePartMatching the use part matching
     * @return the institutes
     */
    public List<Institute> getInstitutes(boolean usePartMatching) {
        if (!usePartMatching) {
            logger.log(Level.INFO, "Institute get data by abbreviation executed");
            return dataSource.getInstitutes();
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    /**
     * Insert institute int.
     *
     * @param institute       the institute
     * @param usePartMatching the use part matching
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    public int insertInstitute(Institute institute,  boolean usePartMatching) throws IOException, SQLException {
        if (!usePartMatching) {
            logger.log(Level.INFO, "institute insert executed");
            return dataSource.insertInstitute(institute);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

}
