package nl.bioinf.spraak.service;

/**
 *  created by Bas Kasemir
 */

import nl.bioinf.spraak.data.DataSource;
import nl.bioinf.spraak.models.Institute;
import nl.bioinf.spraak.models.Praatscript;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Praatscript service.
 */
@Service
public class PraatscriptService {

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    @Value("${praat.praatscript-path}")
    private  String scriptspath;

    /**
     * List all available praatscripts.
     *
     * @return a list of praatscript models
     */
    public List<Praatscript> getAllPraatscripts() {


        File dir = new File(scriptspath);
        FileFilter fileFilter = new WildcardFileFilter("*.praat", IOCase.INSENSITIVE);
        // For taking both .JPG and .jpg files (useful in *nix env)
        File[] fileList = dir.listFiles(fileFilter); if (fileList.length > 0) {
            /** The oldest file comes first **/

            Arrays.sort(fileList, LastModifiedFileComparator.LASTMODIFIED_COMPARATOR);
        } //filesList now contains all the JPG/jpg files in sorted order

        List<Praatscript> praatscriptList = new ArrayList<Praatscript>();

        for (int i = 0; i < fileList.length; i++) {
            System.out.println("fileList = " + fileList[i].getName());
            int id = i+1;
            String filename = fileList[i].getName();
            praatscriptList.add(new Praatscript(id, filename));
        }

        System.out.println("praatscriptList = " + praatscriptList);

        return praatscriptList;
    }

}
