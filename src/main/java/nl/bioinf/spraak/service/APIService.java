package nl.bioinf.spraak.service;

import com.google.gson.Gson;
import nl.bioinf.spraak.applications.SoXRunner;
import nl.bioinf.spraak.auth.JWTTokenGenerator;
import nl.bioinf.spraak.models.*;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.springframework.web.multipart.MultipartFile;

import javax.management.ValueExp;

import static nl.bioinf.spraak.auth.SecurityConstants.HEADER_STRING;
import static nl.bioinf.spraak.auth.SecurityConstants.TOKEN_PREFIX;

/**
 * Api service contacts api.
 */
@Service
public class APIService {

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    @Value(value = "${praat.tempFiles-path}")
    private String tempPath;

    @Value(value = "${sox.path}")
    private String soxPath;

    /**
     * The Jwt token generator.
     */
    @Autowired
    JWTTokenGenerator jwtTokenGenerator;

    /**
     * The User service.
     */
    @Autowired
    UserService userService;

    /**
     * The Institute service.
     */
    @Autowired
    InstituteService instituteService;

    /**
     * The SoX runner.
     */
    @Autowired
    SoXRunner soXRunner;

    /**
     * Make get request string.
     *
     * @param auth       the auth
     * @param principal  the principal
     * @param requestURL the request url
     * @param args       the args
     * @return the string
     * @throws IOException the io exception
     */
    public String makeGetRequest(Authentication auth, Principal principal, String requestURL, List<NameValuePair> args) throws IOException {
        // Generate a token
        String token = jwtTokenGenerator.generateToken(auth, null, null, null);

        // Construct the full url
        String url = getURL(principal, requestURL);

        // Set default response
        String apiResponse = "Error, could not send get request";

        // Set up a new HTTP CLient
        HttpClient client = HttpClientBuilder.create().build();
        // Make a new get request with the submitted arguments
        HttpGet get = new HttpGet(url);
        EntityBuilder builder = EntityBuilder.create();
        builder.setParameters(args);


        // Add the token to the header
        get.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
        logger.log(Level.INFO, "Token has been added to the get request header");

        try {
            // Execute the request and get the response
            HttpResponse APIHTTPResponse = client.execute(get);

            apiResponse = EntityUtils.toString(APIHTTPResponse.getEntity());
        } catch (IOException e) {
            logger.log(Level.WARNING, "IO Exception while getting the Get API response");
            return apiResponse;
        }
        logger.log(Level.INFO, "Get request to API was successfully sent");
        return apiResponse;
    }


    /**
     * Make request string.
     *
     * @param auth       the auth
     * @param principal  the principal
     * @param requestURL the request url
     * @param args       the args
     * @param filesToSend the files list to send
     * @return the string
     * @throws IOException the io exception
     */
    public String makeRequest(Authentication auth, Principal principal, String requestURL, List<NameValuePair> args, List<MultipartFile> filesToSend) throws IOException {
        // Generate a token
        String token = jwtTokenGenerator.generateToken(auth, null,null, null);

        // Construct the full url
        String url = getURL(principal, requestURL);

        // Set the default response
        String apiResponse = "Error, could not send request";

        // Set up a new HTTP Client
        HttpClient client = HttpClientBuilder.create().build();
        // Make a new post request object with the right settings
        HttpPost post = new HttpPost(url);
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

        // Make a new list that will contain all the clipping values
        List<Double> clippingList = new ArrayList<>();

        // Check if there are files to send, if so add it to the body
        if (filesToSend.size() > 0) {
            for (int i = 0; i < filesToSend.size(); i++) {

                // Convert the multipartfile to a File
                File fileToSend = convertMultipartfileToFile(filesToSend.get(i), tempPath);
                BufferedReader br = new BufferedReader(new FileReader(fileToSend));
                if (br.readLine() == null) {
                    logger.log(Level.INFO, "No errors, and file empty");
                }
                // Add the file to the body
                FileBody fileBody = new FileBody(fileToSend, ContentType.DEFAULT_BINARY);
                builder.addPart("file"+(i+1), fileBody);
                logger.log(Level.INFO, "File #" + (i+1) + " has been added to the request body");
                //Check if the file is an wav file
                if(filesToSend.get(i).getContentType().contains("audio/")) {
                    logger.log(Level.INFO, "File #" + (i+1) + " is an audio file ("+filesToSend.get(i).getContentType()+")");
                    // Get the amount of clipping for the file
                    double clipping = getAmountOfClipping(filesToSend.get(i));
                    // Add it to the list
                    clippingList.add(i, clipping);
                }
            }

            // Check for the file id with the lowest clipping
            int lowestClippingID = 0;
            double lowestClippingValue = 0.00;
            // if the "Pk lev dB" is 0, then there is clipping
            for (int i = 0; i < clippingList.size(); i++) {
                if (clippingList.get(i) < lowestClippingValue) {
                    lowestClippingID = i;
                    lowestClippingValue = clippingList.get(i);
                }
            }

            // Add the id to the arguments
            args.add(new BasicNameValuePair("lowest_clipping_file", String.valueOf(filesToSend.get(lowestClippingID).getOriginalFilename())));

        }

        // Add all the arguments to the request
        for (int i = 0; i < args.size(); i++) {

            NameValuePair kv = args.get(i);
            String key = kv.getName();
            String value = kv.getValue();
            builder.addTextBody(key, value, ContentType.DEFAULT_BINARY);

        }


        // Add the token to the header
        post.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
        logger.log(Level.INFO, "Token has been added to the request header");

        // Preform the request and return the response
        try {
            HttpEntity multipart = builder.build();
            post.setEntity(multipart);
            HttpResponse APIHTTPResponse = client.execute(post);

            apiResponse = EntityUtils.toString(APIHTTPResponse.getEntity());
        } catch (IOException e) {
            logger.log(Level.WARNING, "IO Exception while getting the API response");
            return apiResponse;
        }
        logger.log(Level.INFO, "Request to API was successfully sent");
        return apiResponse;
    }

    public static File convertMultipartfileToFile(MultipartFile file, String tempPath) throws IOException {
        File convFile = new File(tempPath + file.getOriginalFilename());

        InputStream initialStream = file.getInputStream();
        byte[] buffer = new byte[initialStream.available()];
        initialStream.read(buffer);

        OutputStream outStream = new FileOutputStream(convFile);
        outStream.write(buffer);
        return convFile;
    }

    /**
     * Function that will return the amount of clipping for a file
     *
     * @param fileToCheck the file to check for the amount of clipping
     * @return the amount of clipping
     * @throws IOException the io exception
     */
    public double getAmountOfClipping(MultipartFile fileToCheck) throws IOException {
        // Get a unique timestamp to prevent duplicates
        String ms = String.valueOf(System.currentTimeMillis());
        // Create bytes array for writing file
        InputStream inputStream = fileToCheck.getInputStream();
        byte[] buffer = new byte[inputStream.available()];
        // Construct a path with the filename for input
        String inputPath = tempPath + ms + "_" + fileToCheck.getOriginalFilename();
        // Construct a path with the filename for output
        String outputPath = tempPath + ms + "_OUT_" + fileToCheck.getOriginalFilename();
        logger.log(Level.INFO, "Saving file to temp dir ("+ms + "_" + fileToCheck.getOriginalFilename()+")");
        // Write the file
        File targetFile = new File(inputPath);
        try (FileOutputStream fos = new FileOutputStream(targetFile) ) {
            fos.write(fileToCheck.getBytes());
            fos.flush();
        }

        logger.log(Level.INFO, "Running SOX over file "+ms + "_" + fileToCheck.getOriginalFilename());
        String valueToGet = "Pk lev dB";
        String soxOutput = soXRunner.commandLineRunnerSox(inputPath, outputPath, valueToGet);

        File outFile = new File(outputPath);
        // Try to delete the file
        try {
            if(targetFile.delete()) {
                logger.log(Level.INFO, targetFile.getName() + " is deleted");
            } else {
                logger.log(Level.WARNING, targetFile.getName() + " Could not be deleted");
            }
            if(outFile.delete()) {
                logger.log(Level.INFO, outFile.getName() + " is deleted");
            } else {
                logger.log(Level.WARNING, outFile.getName() + " Could not be deleted");
            }
        } catch(Exception e) {
            e.printStackTrace();
        }

        double soxValue = 0;

        String[] soxOutMinimized = soxOutput.trim().split(valueToGet);
        Pattern pattern = Pattern.compile("^(\\S\\d{1}.\\d{2})|^(\\d{1}.\\d{2})");
        Matcher matcher = pattern.matcher(soxOutMinimized[1]);
        if (matcher.find())
        {
            soxValue = Double.parseDouble(matcher.group(1));
        } else {
            logger.log(Level.WARNING, "");
        }

        return soxValue;
    }

    /**
     * Store praat results string.
     *
     * @param auth          the auth
     * @param principal     the principal
     * @param requestURL    the request url
     * @param args          the args
     * @param spraakResults the spraak results
     * @return the string
     * @throws IOException the io exception
     */
    public String storePraatResults(Authentication auth, Principal principal, String requestURL, List<NameValuePair> args, SpraakResults spraakResults) throws IOException {

        // Backup the soundfile data
        File soundFileData = spraakResults.getSoundFileData();

        // Empty the soundfile so it can be sended with the JSON as string
        spraakResults.setSoundFileData(null);

        // Generate a JSON object of the model
        Gson gson = new Gson();
        String jsonString = gson.toJson(spraakResults);

        // Add the JSON object
        args.add(new BasicNameValuePair("spraakResults", jsonString));

        // Preform the request
        String apiResponse = makeRequest(auth, principal, requestURL, args, null);

        return apiResponse;
    }

    public String storeOpenSmileResults(Authentication auth, Principal principal, String requestURL, List<NameValuePair> args, OpenSmileResults openSmileResults) throws IOException {

        // Backup the soundfile data
        File openSmileData = openSmileResults.getOpenSmileResult();

        // Empty the soundfile so it can be sended with the JSON as string
        openSmileResults.setOpenSmileResult(null);

        // Generate a JSON object of the model
        Gson gson = new Gson();
        String jsonString = gson.toJson(openSmileResults);

        // Add the JSON object
        args.add(new BasicNameValuePair("openSmileResults", jsonString));

        // Preform the request
        String apiResponse = makeRequest(auth, principal, requestURL, args, null);

        return apiResponse;
    }



    /**
     * Gets url.
     *
     * @param principal  the principal
     * @param requesturl the requesturl
     * @return the url
     * @throws UnsupportedEncodingException the unsupported encoding exception
     */
    public String getURL(Principal principal, String requesturl) throws UnsupportedEncodingException {

        // Get the username and its details
        String username = principal.getName();
        User userData = userService.getUserByUserName(username, false);

        // Get the institue abbreviation for lookup in the database
        String userInstituteAbbr = userData.getInstitute_abbr();
        Institute instituteData = instituteService.getInstituteByAbbreviation(userInstituteAbbr, false);

        // Set the API host
        String apiHost = instituteData.getApi_host();

        // Decode the url
        String decodedURL = java.net.URLDecoder.decode(requesturl, "UTF-8");

        // TODO: Add https as protocol when moving to production
        // Construct the full URL
        String requestURL = "http://" + apiHost + decodedURL;
        logger.log(Level.INFO, "Request URL has been generated: "+requestURL);

        return requestURL;
    }
}
