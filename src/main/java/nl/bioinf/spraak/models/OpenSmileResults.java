package nl.bioinf.spraak.models;

import java.io.File;
import java.sql.Blob;

public class OpenSmileResults {

    private int id;
    private String patientNumber;
    private String userName;
    private String fileName;
    private File openSmileResult;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPatientNumber() {
        return patientNumber;
    }

    public void setPatientNumber(String patientNumber) {
        this.patientNumber = patientNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public File getOpenSmileResult() {
        return openSmileResult;
    }

    public void setOpenSmileResult(File openSmileResult) {
        this.openSmileResult = openSmileResult;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public OpenSmileResults(int id, String patientNumber, String userName, File openSmileResult, String fileName) {
        this.id = id;
        this.patientNumber = patientNumber;
        this.userName = userName;
        this.openSmileResult = openSmileResult;
        this.fileName = fileName;
    }

    public OpenSmileResults() {
    }
}