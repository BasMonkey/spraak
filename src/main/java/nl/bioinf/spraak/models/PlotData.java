package nl.bioinf.spraak.models;

import java.util.List;

public class PlotData {

    private List<String> praatResult;
    private List<String> openSmileResult;
    private List<String> praatControlData;
    private List<String> openSmileControlData;

    public List<String> getPraatResult() {
        return praatResult;
    }

    public void setPraatResult(List<String> praatResult) {
        this.praatResult = praatResult;
    }

    public List<String> getOpenSmileResult() {
        return openSmileResult;
    }

    public void setOpenSmileResult(List<String> openSmileResult) {
        this.openSmileResult = openSmileResult;
    }

    public List<String> getPraatControlData() {
        return praatControlData;
    }

    public void setPraatControlData(List<String> praatControlData) {
        this.praatControlData = praatControlData;
    }

    public List<String> getOpenSmileControlData() {
        return openSmileControlData;
    }

    public void setOpenSmileControlData(List<String> openSmileControlData) {
        this.openSmileControlData = openSmileControlData;
    }

    public PlotData(List<String> praatResult, List<String> openSmileResult, List<String> praatControlData, List<String> openSmileControlData) {
        this.praatResult = praatResult;
        this.openSmileResult = openSmileResult;
        this.praatControlData = praatControlData;
        this.openSmileControlData = openSmileControlData;
    }

    public PlotData() {
    }
}
