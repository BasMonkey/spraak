package nl.bioinf.spraak.models;

public class LoginAttempts {

    private int id;
    private String username;
    private int number_of_wrong_attempts;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getNumber_of_wrong_attempts() {
        return number_of_wrong_attempts;
    }

    public void setNumber_of_wrong_attempts(int number_of_wrong_attempts) {
        this.number_of_wrong_attempts = number_of_wrong_attempts;
    }

    public LoginAttempts(int id, String username, int number_of_wrong_attempts) {
        this.id = id;
        this.username = username;
        this.number_of_wrong_attempts =  number_of_wrong_attempts;
    }

    public LoginAttempts() {

    }
}
