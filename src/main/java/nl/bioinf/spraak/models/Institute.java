package nl.bioinf.spraak.models;

/**
 *  created by Bas Kasemir
 */

public class Institute {

    private String abbreviation;
    private String full_name;
    private String api_host;


    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getApi_host() {
        return api_host;
    }

    public void setApi_host(String api_host) {
        this.api_host = api_host;
    }



    public Institute(String abbreviation, String full_name, String api_host) {
        this.abbreviation = abbreviation;
        this.full_name = full_name;
        this.api_host =  api_host;
    }

    public Institute(){

    }


}
