package nl.bioinf.spraak.models;

/**
 *  created by Bas Kasemir
 */

import java.sql.Blob;

public class SoundFileData {

    private int id;
    private String filename;
    private Blob soundFileData;
    private String soundFileDataBase64;
    private String channel;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Blob getSoundFileData() {
        return soundFileData;
    }

    public void setSoundFileData(Blob soundFileData) {
        this.soundFileData = soundFileData;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSoundFileDataBase64() {
        return soundFileDataBase64;
    }

    public void setSoundFileDataBase64(String soundFileDataBase64) {
        this.soundFileDataBase64 = soundFileDataBase64;
    }

    public SoundFileData(int id, String filename, Blob soundFileData, String soundFileDataBase64, String channel) {
        this.id = id;
        this.filename = filename;
        this.soundFileData = soundFileData;
        this.soundFileDataBase64 = soundFileDataBase64;
        this.channel = channel;
    }

    public SoundFileData(){

    }


}