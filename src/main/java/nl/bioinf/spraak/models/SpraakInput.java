package nl.bioinf.spraak.models;

/**
 *  created by Marcel Zandberg
 */

import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;

public class SpraakInput {

    private String patNumber;
    private String date;
    private String praat_advanced;
    private String cutAudio;
    private String normalizeAudio;
    private String typeScript;
    private String fileName;
    private String randomHash;


    public String getPatNumber() {
        return patNumber;
    }

    public void setPatNumber(String patNumber) {
        this.patNumber = patNumber;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPraat_advanced() {
        return praat_advanced;
    }

    public void setPraat_advanced(String praat_advanced) {
        this.praat_advanced = praat_advanced;
    }

    public String getTypeScript() {
        return typeScript;
    }

    public void setTypeScript(String typeScript) {
        this.typeScript = typeScript;
    }

    public String getCutAudio() {
        return cutAudio;
    }

    public void setCutAudio(String cutAudio) {
        this.cutAudio = cutAudio;
    }

    public String getNormalizeAudio() {
        return normalizeAudio;
    }

    public void setNormalizeAudio(String normalizeAudio) {
        this.normalizeAudio = normalizeAudio;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getRandomHash() {
        return randomHash;
    }

    public void setRandomHash(String randomHash) {
        this.randomHash = randomHash;
    }
}
