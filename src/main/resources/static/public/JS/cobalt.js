/* Copyright 2020 Bas Kasemir */

window.cobalt = (function () {
    function cobalt (els) {
        if (typeof jQuery == 'undefined') {
            console.error("JQuery is not loaded. Please include it in a line before the Cobalt script.")
        }
    }

    var cobalt = {
        make: function (options) {
            if (options == null) {
                console.error("No options specified");
                return
            }
            destroy();
            make(options)
        },

        destroy: function () {
            destroy()
        }
    };

    return cobalt;
}());

function make(options) {

    let type = options.type;
    let icon = options.icon;
    let title = options.title;
    let data = options.data;
    let showButton = options.showButton;
    let buttonColor = options.buttonColor;
    let buttonText = options.buttonText;
    let showCancelButton = options.showCancelButton;
    let cancelButtonColor = options.cancelButtonColor;
    let cancelButtonText = options.cancelButtonText;

    if (icon == null || icon === "") {
        switch (type) {
            case "error":
                icon = "fas fa-times";
                break;

            case "info":
                icon = "fas fa-info-circle";
                break;

            case "warning":
                icon = "fas fa-exclamation-triangle";
                break;

            case "success":
                icon = "fas fa-check";
                break;

            case "loading":
                icon = "fas fa-sync-alt";
                break;
        }
    }

    let regexPath = new RegExp(
        "^(?:http(s)?:\\/\\/)?[\\w.-]+(?:\\.[\\w\\.-]+)+[\\w\\-\\._~:/?#[\\]@!\\$&'\\(\\)\\*\\+,;=.]+$"
    );

    let regexFA = new RegExp(
        "^(fas fa-\\w*|fab fa-\\w*|fal fa-\\w*)"
    );

    let iconData;

    if (regexFA.test(icon)) {
        iconData = `<i class="`+ icon +` icon"></i>`;
    } else {
        iconData = `<img src="`+ icon +`" height="50" width="50" alt="icon" class="icon">`;
    }

    let buttonsData;
    let buttonData;
    let cancelButtonData;

    let showButtonCheck = showButton === true && showButton !== undefined;
    let textButtonCheck = buttonText != null && buttonText !== "";
    if (showButtonCheck || textButtonCheck) {

        if (buttonText == null || buttonText === "") {
            buttonText = "OK"
        }

        if (cancelButtonText == null || cancelButtonText === "") {
            cancelButtonText = "CANCEL"
        }

        if (buttonColor == null || buttonColor === "") {
            buttonColor = "rgba(76, 209, 55,1.0)"
        }

        if (cancelButtonColor == null || cancelButtonColor === "") {
            cancelButtonColor = "rgba(235, 59, 90,1.0)";
        }

        buttonData = `<div class="cblt-btn" style="background-color: `+ buttonColor +`">`+ buttonText+`</div>`;

        cancelButtonData = `<div class="cblt-btn" style="background-color: `+ cancelButtonColor +`">`+ cancelButtonText+`</div>`;

        buttonsData = `<div class="cblt-btn-wrapper">` + cancelButtonData + buttonData + `</div>`;

    } else {
        buttonsData = "";
    }

    $("body").append(`
<div class="cblt">
    <div class="cblt-box cblt-`+ type +`">
        `+ iconData +`
        <h3>`+ title +`</h3>
        <p>`+ data +`</p>
        
        ` + buttonsData + `
    </div>
</div>`);

    $('html').addClass("disablescroll");
}

function destroy() {
    $('div[class="cblt"]').remove();

    $('html').removeClass("disablescroll");
}

$( document ).ready( function () {
    $("head").append('<link href="/public/CSS/cobalt.css" rel="stylesheet" type="text/css">');
});