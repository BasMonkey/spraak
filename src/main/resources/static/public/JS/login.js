/**
 * Created by Bas Kasemir
 */

$(document).ready( function () {

    $("#LoginForm, #ResetForm").submit( function (e) {
        // Check if an 2FA code has been entered
        if ( $("#2FACode").val().length < 6) {
            e.preventDefault();
            // Hide
            $('#credentials, #forgotPassword').fadeOut(375);
            $("#2fa").delay(375).fadeIn(375);
            setTimeout(
                function()
                {
                    $("#2fa1").focus();
                }, 750);
            return
        }

    });

    $(".GoToCredentials").click( function () {
        $("#2fa, #password-reset-form").fadeOut(750);
        $('#credentials, #greeting, #forgotPassword, .notification-login-container').delay(750).fadeIn(750);
        $("#LoginForm #username").focus();
    });

    $("#forgotPassword").click( function () {
        $("#2fa").fadeOut(750);
        $("#credentials, #greeting, #forgotPassword, .notification-login-container").fadeOut(750);
        $('#password-reset-form').delay(750).fadeIn(750);
        $("#password-reset-form #pwr-username").focus();
    });

    /*$('input[type="tel"]').focus( function () {
        if ($(this).prev()val === 'input') {
            if ( $(this).prev().val().length < 1) {
                // Previous box has not a value
                $(this).prev().focus();
            }
        }
    });*/

    // Initaite a new variable for the value on key down
    let value = "";

    $(".goToMaxLen").keydown( function (e) {
        // Store the value of the input field before any data is entered
        value = $(this).val();
        
        // If user doesnt enter a number, do nothing
        if (!(e.key >= 0) || !(e.key < 10)) {
            e.preventDefault();
            return
        }
    });

    $('.goToMaxLen').keyup(function (e) {

        // get # of chars in the input field
        let length = $(this).val().length;
        // get maxlength value
        let maxlen = $(this).attr('maxlength');

        let code2FA = $("#2fa1").val()+$("#2fa2").val()+$("#2fa3").val()+$("#2fa4").val()+$("#2fa5").val()+$("#2fa6").val();

        // Combine the 2fa fields
        $("#2FACode").val(code2FA);

        // Ccheck if the code is fully entered. If so, submit the form
        if(code2FA.length == 6) {
            console.log("Submitting");
            $("#2fa").hide();
            $("#spinner").removeClass("hideonload");
            $("#LoginForm, #ResetForm").submit();
        }

        // when backspace is pressed check if the cursor needs to move to the previous input field
        if (e.keyCode == 8) {
            if (value === "") {
                // Field is empty already, set focus to previous field and make that one empty
                $(this).prev().val("").focus();
                $("#2FACode").val($("#2fa1").val()+$("#2fa2").val()+$("#2fa3").val()+$("#2fa4").val()+$("#2fa5").val()+$("#2fa6").val());
            }

        } else {
            if (value !== "") {
                //Check if the pressed key is a number key
                if (e.key >= 0 || e.key < 10) {
                    $(this).next().val(e.key);
                    $("#2FACode").val($("#2fa1").val()+$("#2fa2").val()+$("#2fa3").val()+$("#2fa4").val()+$("#2fa5").val()+$("#2fa6").val());
                }
            }
            if (length == maxlen) {
                // if the max length is reached, set the focus on the next input
                $(this).next().focus();
            }
        }
    });
});