// Copyright 2020 Bas Kasemir

$( document ).ready( function () {

    var applinks = {};

    applinks.ios = "https://itunes.apple.com/app/authy/id494168017?mt=8";
    applinks.android = "https://play.google.com/store/apps/details?id=com.authy.authy&hl=en";

    $("#AuthyApp").click( function () {
        swal({
            html: true,
            icon: null,
            title: null,
            text: '<p>'+activation.applink_mobile+'</p><br>' +
                '<div class="appQR-div">' +
                    '<canvas class="appQR" id="QR-ios" height="100.5" width="100.5"></canvas>' +
                    '<h3><i class="fab fa-app-store-ios"></i><br>App Store</h3>' +
                '</div> ' +

                '<div class="appQR-div">' +
                    '<canvas class="appQR" id="QR-android" height="100.25" width="100.25"></canvas>' +
                    '<h3><i class="fab fa-google-play"></i><br>Google Play</h3>' +
                '</div>' +

                '<br><br><br>' +

                '<p>'+activation.applink_desktop+'</p><br>' +

                '<a href="https://authy.com/download/"><div class="btn"><i class="fas fa-laptop"></i>&nbsp;Mac / PC Download</div>',
            showCancelButton: false,
            footer: '<p class="copy">Footer</p>',
            allowOutsideClick: true,
            confirmButtonText: activation.done_button,
            confirmButtonColor: spraak_colors.royal_blue
        });

        for (let item in applinks) {
            let selector = "#QR-"+item;
            $( selector ).qrcode({
                render: 'canvas',
                minVersion: 1,
                maxVersion: 40,
                ecLevel: 'M',
                size: 100,
                left: 0,
                top: 0,
                mode: 0,
                mSize: 0.1,
                mPosX: 0.5,
                mPosY: 0.5,
                quiet: 0,
                fill: "#000",
                background: null,
                text: applinks[item]
            });
        }


    });

});