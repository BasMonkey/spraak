// Copyright 2020 Marcel Zandberg during internship


$(document).ready( function () {

    // Clear the dataobject on load
    dataobj = null;

    // Fetch all the results
    getAllResults();

    // Set the select input of the page # shown to 1
    $("#pageSelector").val("1");


});

$(document).on('click', '.delete', function (e) {
    //delete rundant audio files
    var session = $(this).attr("data-session");

    var id = $(this).attr("id");
    id = "#row"+id;

    //fire warning before event
    Swal.fire({
        title: 'Are you sure you want to delete this session?',
        text: "You won't be able to revert it",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#4cd137',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Delete it',
        showLoaderOnConfirm: true,
        closeOnConfirm: false,
        footer: '<p class="copy">Timestamp: ' + new Date() + '</p>',
        allowOutsideClick: false
    }).then(function (value) {

        if(value){

            $.get( "/delete/result?session="+session).done(function( data ) {
                //remove list element from list
                console.log(data);
                if (data === true){
                    $(id).hide("slow");
                }

            });
        }
    });


});



// Function that will render the results based on the data JSON object it recieves as param
function renderResults(data) {


    // Empty the list with tiems so we don't get duplicates shown
    $("#items-list").empty();

    // Check if there is even any data. If not, show the bummer message. If so, hide it.
    if (data.length < 1) {
        $("#bummer").show();
        return;
    } else {
        $("#bummer").hide();
    }

    //
    $("#allPerPageOpt").attr('value', data.length);


    for (var i = 0; i < data.length; i++) {

        if (i < data.length) {
            var result = data[i];
            //session.substring(0, session.lastIndexOf("_"))
            var rowdata = '<li class="result-row" data-result-id="'+result.id+'" id="'+"row"+result.id+'"></div><div class="row"><div class="session">' + result.session.substring(0, result.session.lastIndexOf("_")) + '</div><div class="fileAmount">' + result.fileAmount + '</div><div class="timeStamp">'+result.timeStamp+'</div><div class="owner">'+result.owner+'</div></div>';

            var endofrow = '<div class="delete" data-session="'+result.session+'" id="'+result.id+'"><i class="fas fa-trash"></i></div><a href="http://localhost:8080/download/selected?sessions=' + result.session + '"><div class="share"><i class="fas fa-arrow-alt-circle-down"></i></div></a></div></li>';
            $("#items-list").append(rowdata + endofrow);
        } else {
            return;
        }
    }

}



function renderPageSelect(data) {

    $("#pageSelector").empty();
    $("#amountOfResults").text(data.length);

    var numPerPage = $("#numPerPage").val();

    var nOfPages = Math.ceil((data.length/numPerPage))+1;


    $("#numOfPages").text(nOfPages-1);

    for (var i = 1; i < nOfPages; i++) {
        $("#pageSelector").append('<option value="'+Number(i-1)+'">'+i+'</option>');
    }
}


/*******************
 * FETCH FUNCTIONS *
 *******************/

function getAllResults() {

    //var url = encodeURIComponent("/results/get/");
    $.get("/api/results/").done(function( data ) {
        renderResults(data);
        renderPageSelect(data);
    });

}
