// Copyright 2020 Marcel Zandberg

$(document).ready(  function () {
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");

    var preproces = "";
    var usePraat = "";
    var useOpensmile = "";
    var praatscript = "";

    // Make a new formdata
    var myFormData = new FormData();


    /*******************
     * PROGRAM SETTINGS *
     *******************/

    //switch to use praat or not

    $('#checkbox-result-praat').click( function() {

        if($('#checkbox-result-praat').hasClass("checked")){
            $('#plot-time-praat').removeClass("hideplot");
            $('#plot-box-praat').removeClass("hideplot");
            usePraat += "1";
        }else {
            $('#plot-time-praat').addClass("hideplot");
            $('#plot-box-praat').addClass("hideplot");
            usePraat = "";
        }
    });

    //switch to use opensmile or not
    $('#checkbox-result-opensmile').click( function() {

        if($('#checkbox-result-opensmile').hasClass("checked")){
            $('#plot-time-opensmile').removeClass("hideplot");
            $('#plot-box-openSmile').removeClass("hideplot");
            useOpensmile += "1";
        }else {
            $('#plot-time-opensmile').addClass("hideplot");
            $('#plot-box-openSmile').addClass("hideplot");
            useOpensmile = "";
        }
    });



    //shows submit btn when at least one program is selected
    $('.checkbox-result-programs').click( function () {
        if($('.checkbox-result-programs').hasClass("checked")){
            $(".files-btn").removeClass("hideonload-important");
        }else {
            $(".files-btn").addClass("hideonload-important");
        }
    });

    //switch to use preproceccing or not
    $('.checkbox-preproces').click( function () {
        if($('.checkbox-preproces').hasClass("checked")){
            $("#procesoption").removeClass("hideonload-important");
            preproces += "1";
        }else {
            $("#procesoption").addClass("hideonload-important");
            preproces = "";
        }
    });

    $('#praat-script').change(function () {
        praatscript = $(this).val();
    });




    /********************
     * RUN THE PROGRAMS *
     ********************/
    //upload form
    $("#uploadButton").click( function(e) {
        //check if scroller needs to be hidden
        if(usePraat === "" || useOpensmile === ""){
            $("#timesection").addClass("hideonload");
            $("#boxsection").addClass("hideonload");
        }

        //show plot uppon which selection is made
        if(patnr.length > 1){
            $('#boxsection').removeClass("hideonload-important");
            $('#timesection').addClass("hideonload-important");
            $('#ctrl').removeClass("hideplot");
            $('#time').addClass("hideplot");
        }
        // Prevent the default browser action
        e.preventDefault();
        // Get the original data of the submit button (so we can replace it with the right icon and locale when an
        // error occured.
        var original_btn_data = $(this).html();
        // Add all the entered data to the formData object.
        myFormData.append("usePraat", usePraat);
        myFormData.append("useOpensmile", useOpensmile);
        myFormData.append("fileAmount",patids);
        myFormData.append("preproces", preproces);
        myFormData.append("praatscript", praatscript);
        myFormData.append("randomHash", randomHash);
        myFormData.append("channelL", channelL.toString());
        myFormData.append("channelR", channelR.toString());


        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to stop the analysis!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#4cd137',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirm!',
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            footer: '<p class="copy">Timestamp: ' + new Date() + '</p>',
            allowOutsideClick: false
        }).then(function (value) {
            console.log(value["value"]);

            if(value["value"]){
                Swal.fire({
                    title: 'Loading',
                    text: 'Downloading and processing files',
                    footer: '<p class="copy">Timestamp: ' + new Date() + '</p>',
                    allowEscapeKey: false,
                    allowOutsideClick: false,
                    onOpen: () => {
                    Swal.showLoading();
                    }
                });
                //get sound files
                var url = encodeURIComponent("/api/v1/results/soundfile/get/id/" + patids);
                $.get( "/api/download-internal?url="+url+"&randomHash="+ randomHash).done(function( data ) {
                    //check if files are downloaded
                    if(data === true){
                        // Make an Ajax request and handle success / errors
                        $.ajax({
                            url: '/visuals/uploadVisuals',
                            type: 'POST',
                            data: myFormData,
                            processData: false,
                            contentType: false,
                            headers: {
                                'X-CSRF-TOKEN': token
                            },
                            dataType: 'text',
                            success: function (data) {

                                //alert when results are being rendert
                                if (data === "Uploaded") {


                                    Swal.fire({
                                        type: 'success',
                                        title: 'Process complete!',
                                        text: 'Fetching the results',
                                        footer: '<p class="copy">Timestamp: ' + new Date() + '</p>',
                                        timer: 1500,
                                        timerProgressBar: true,
                                        allowOutsideClick: false,
                                        showConfirmButton: false
                                    }).then(function (result) {
                                        if (!result.value) {
                                            //get the data
                                            $.get( "/download/get?randomHash="+randomHash+"&usePraat="+usePraat+"&useOpenSmile="+useOpensmile+"&plot="+patnr.length).done(function( data ) {
                                                getData(data)
                                            });
                                            // Fill the progress bar
                                            showByNumber(curr_form_progress+1);
                                            //delete rundant audio files
                                            $.get( "/delete/audio?randomHash="+randomHash).done(function( data ) {
                                                getData(data)
                                            });
                                        } else {
                                            window.location.reload();
                                        }
                                    });
                                    //catch for when no programs are selected
                                } else if (data === "no_programs"){
                                    Swal.fire({
                                        type: 'error',
                                        title: "Oeps",
                                        text: "Je hebt geen programma geselecteerd",
                                        footer: '<p class="copy">Timestamp: ' + new Date() + '</p>',
                                        showCancelButton: false,
                                        focusConfirm: false,
                                        confirmButtonColor: spraak_colors.royal_blue,
                                        confirmButtonText: "Ik ga het opnieuw proberen"
                                    }).then(function(result) {
                                        $("#uploadButton").removeClass('cur-wait').html(original_btn_data);
                                        window.location.reload();
                                    });
                                    //catch for unknown error
                                }else {
                                    Swal.fire({
                                        type: 'error',
                                        title: analyse.error,
                                        text: analyse.error_msg,
                                        footer: '<p class="copy">Timestamp: ' + new Date() + '</p>',
                                        showCancelButton: false,
                                        focusConfirm: false,
                                        confirmButtonColor: spraak_colors.royal_blue,
                                        confirmButtonText: analyse.try_again
                                    }).then(function(result) {
                                        $("#uploadButton").removeClass('cur-wait').html(original_btn_data);
                                        window.location.reload();
                                    });
                                }
                            },
                            //catch for xhr error
                            error: function(xhr) {
                                $(".form-progress-bar div").css('background-color', 'red');
                                Swal.fire({
                                    type: 'error',
                                    title: analyse.error,
                                    text: analyse.error_msg,
                                    footer: '<p class="copy">An error occured: ' + xhr.status + ' ' + xhr.statusText + ' | Timestamp: '+ new Date() + '</p>',
                                    showCancelButton: false,
                                    focusConfirm: false,
                                    confirmButtonColor: spraak_colors.royal_blue,
                                    confirmButtonText: analyse.try_again
                                }).then(function(result) {
                                    // Restore the original submit button data

                                    $("#uploadButton").removeClass('cur-wait').html(original_btn_data);
                                    window.location.reload();
                                });
                            }
                        });
                    }else {
                        Swal.fire({
                            type: 'error',
                            title: "Oeps",
                            text: "Je hebt geen programma geselecteerd",
                            footer: '<p class="copy">Timestamp: ' + new Date() + '</p>',
                            showCancelButton: false,
                            focusConfirm: false,
                            confirmButtonColor: spraak_colors.royal_blue,
                            confirmButtonText: "Ik ga het opnieuw proberen"
                        }).then(function(result) {
                            $("#uploadButton").removeClass('cur-wait').html(original_btn_data);
                            window.location.reload();
                        });
                    }
                });

            }else{
                window.location.reload();
            }

        });

    });
});


/*********************
 *NAVIGATE TO PLOT*
 *********************/

//show plot tab after
function showByNumber( number ) {

    // Check if the requested number is outside of the available sections. If so, do nothing.
    //curr_form_progress >= num_of_steps
    if (number < 0 || number > num_of_steps) {
        return;
    }

    // If the user is not allowed to view this section yet, show an alert.
    if ( number > maxSectionNum) {
        // Timeout is required to work with the return statement. Otherwise the alert would disappear immediately.
        setTimeout(function() {
            Swal.fire({
                type: 'warning',
                title: analyse.complete_section,
                text: analyse.complete_section_msg,
                showCancelButton: false,
                footer: '<p class="copy">'+analyse.self_closing+'</p>',
                timer: 5000
            });
        },50);
        return;
    }

    // If the user is not allowed to go futher yet, show an alert.
    if (!mayGoFuther) {
        // Timeout is required to work with the return statement. Otherwise the alert would disappear immediately.
        setTimeout(function() {
            Swal.fire({
                type: 'warning',
                title: analyse.complete_section,
                text: analyse.complete_section_msg,
                showCancelButton: false,
                footer: '<p class="copy">'+analyse.self_closing+'</p>',
                timer: 5000
            });
        },50);
        return;
    }

    // Set the animation_completed boolean to false so that a next invoked animation wouldn't occur and mesh up the
    // current animation
    anim_completed = false;

    // Construct a selector for the next section div
    var next_form_step = number;
    var selectorn = "#" + step_ids[next_form_step];

    if ( number > curr_form_progress ) {
        // Set the direction and amount of pixels to travel to the top in the animation
        // Since this value is relative negative, it will look like the user goes down a cylinder.
        var topvalue = '-=250px';

    } else if ( number < curr_form_progress ) {
        // Set the direction and amount of pixels to travel to the bottom in the animation
        // Since this value is relative positive, it will look like the user goes up a cylinder.
        var topvalue = '+=250px';
        $(selectorn).css('top', '-45px');

    } else {
        // If the current view should be shown, complete the animation
        anim_completed = true;
        return
    }

    // Construct a selector for the current section div
    var selectorc = "#"+step_ids[curr_form_progress];

    // Animate the current div
    $( selectorc ).animate({
        top: topvalue,
        opacity: 0
    },{
        duration: anim_speed_prev,
        easing: 'easeInOutQuad'
    });

    // Animate the next section div with the delay as defined in the top of this document. Since this is the last
    // animation, the progress should be updated as well after this animation is completed.
    setTimeout( function () {
        $(selectorn).show().animate({
            top: '205px',
            opacity: 1
        }, {
            duration: anim_speed_next,
            easing: 'easeInOutQuad'
        });
        curr_form_progress = Number(number);

        progress = ((curr_form_progress+1)/(num_of_steps+1))*100;
        $(".form-progress-bar div").css('width', progress-(((1/(num_of_steps+1))*100)*0.5)+'%');
        // $(".form-progress-bar div").css('background-color', spraak_colors.royal_blue);
        // $(".form-progress-bar div").css('width', '100%');
        $(selectorn).children(".question").hide();
        $(selectorn + " > .question").eq(0).show();

        setTimeout( function () {
            anim_completed = true;
        }, anim_completed_time);

    }, anim_fade_delay);

}