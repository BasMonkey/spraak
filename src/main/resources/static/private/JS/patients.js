// Copyright 2019 (2020 mod) Bas Kasemir

var dataobj;

$(document).ready( function () {

    // Clear the dataobject on load
    dataobj = null;

    // Fetch all the results
    getAllResults();

    // Set the select input of the page # shown to 1
    $("#pageSelector").val("1");

    $(function() {

        // Init the datepicker
        $('input[name="datefilter"]').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        // Event handler for the date picker when clicked on the apply button
        $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            filterDelegator();
        });

        // Event handler for the date picker when clicked on the apply button
        $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            filterDelegator();
        });

    });

    // Run the filterDelegator function when the input field for the date changes
    // A keyup is here not necessarily since the date is mostly applied by clicking on the dateselector and the date
    // would be invalid on every keyup.
    $('input[name="datefilter"]').change( function () {
        filterDelegator();
    });

    // Run the filterDelegator function when the user has typed a new character in the input field for the patient ID
    $('input[name="patidfilter"]').keyup( function () {
        filterDelegator();

    });

    // Run the (un)select all checkbox function if necessarily after clicking on the top checkbox
    $('input[name="select-all"]').click( function () {
        if ($(this).prop('checked')) {
            selectAllListedCheckboxes();
        } else {
            unselectAllListedCheckboxes();
        }
    });

    // Render the results with the last retrieved dataobject after the user has changed the current page #
    $('#pageSelector').change( function () {
        renderResults(dataobj);
    });

    // Render the results and check how many pages are needed in the new situation with the last retrieved dataobject
    // after the user has changed the # of items per page
    $('#numPerPage').change( function () {
        renderPageSelect(dataobj);
        renderResults(dataobj);
    });


});

// Event handler for when the user clicks on the view symbol of a result
// "Normal" $.click() doesn't work since the elements are added after the document is loaded
$(document).on('click', '.result-row > .view', function(e) {
    var dataid = $(this).parent().attr('data-result-id');
    window.open("/results/view/"+dataid,"_self")
});

// Function that should set each checkbox to the selected state
function selectAllListedCheckboxes() {
    $( ".result-checkbox" ).each(function() {
        $(this).attr('checked', '').prop('checked').val(1);
    });
}

// Function that should set each checkbox to the unselected state
function unselectAllListedCheckboxes() {
    $( ".result-checkbox" ).each(function() {
        $(this).removeAttr('checked').prop('').val(0);
    });
}

// Function that do some checks to decide which endpoint of the API should be requested
function filterDelegator() {

    // Get the value of the patient ID filter input field
    var patid = $('input[name="patidfilter"]').val();

    // Get the value of the date filter input field
    var date = $('input[name="datefilter"]').val();

    if (patid.length < 1 && date.length < 1) {
        // No filter exists
        getAllResults();
        return;
    }

    if (patid.length < 1 && date.length > 0) {
        // Only date filter exists
        getAllDateResults();
        return;
    }

    if (patid.length > 0 && date.length < 1) {
        // Only patid filter exists
        getPatIDResults();
        return;
    }

    if (patid.length > 0 && date.length > 0) {
        // Both patid and date filter exists
        getPatIDDateResults();
        return;
    }

}

// Function that will render the results based on the data JSON object it recieves as param
function renderResults(data) {

    // "Back up" the dataobj in a new variable
    dataobj = data;

    // Get the current page
    var currentPage = $("#pageSelector").val();

    // Empty the list with tiems so we don't get duplicates shown
    $("#items-list").empty();

    // Check if there is even any data. If not, show the bummer message. If so, hide it.
    if (data.length < 1) {
        $("#bummer").show();
        return;
    } else {
        $("#bummer").hide();
    }

    //
    $("#allPerPageOpt").attr('value', data.length);

    var numPerPage = $("#numPerPage").val();

    var listRangeStart = Number(currentPage*numPerPage);

    var listRangeEnd = Number(Number(listRangeStart)+Number(numPerPage));

    for (var i = listRangeStart; i < listRangeEnd; i++) {

        if (i < data.length) {
            var result = data[i];
            var rowdata = '<div class="result-row-container"><li class="result-row" data-result-id="' + result.id + '"><div class="checker"><input type="checkbox" class="result-checkbox" value="'+result.id+'"></div><div class="data">' + result.patient_number + '</div><div class="data">' + result.sex + '</div><div class="data">' + result.birthdate + '</div><div class="data">' + result.degree + '</div><div class="data">' + result.diag_group + '</div><div class="data">' + result.added_by + '</div></li>';

            var endofrow = '<div class="actions"><div class="delete"><i class="fas fa-trash"></i></div><div class="view view-result"><i class="fas fa-eye"></i></div></div></div>';
            $("#items-list").append(rowdata + endofrow);
        } else {
            return;
        }
    }

}

function changePageView() {

    var currentPage = $("#pageSelector").val();

    console.log(currentPage);

    console.log("dataobj = "+dataobj);

    renderResults(dataobj);

}

function renderPageSelect(data) {

    $("#pageSelector").empty();
    $("#amountOfResults").text(data.length);

    var numPerPage = $("#numPerPage").val();

    var nOfPages = Math.ceil((data.length/numPerPage))+1;

    // Should return 21 pages

    $("#numOfPages").text(nOfPages-1);

    for (var i = 1; i < nOfPages; i++) {
        $("#pageSelector").append('<option value="'+Number(i-1)+'">'+i+'</option>');
    }
}


/*******************
 * FETCH FUNCTIONS *
 *******************/

function getAllResults() {

    var url = encodeURIComponent("/api/v1/patient/get/all/");
    $.get( "/api?url="+url ).done(function( data ) {
        renderResults(data);
        renderPageSelect(data);
    });

}

function getAllDateResults() {

    var date = $('input[name="datefilter"]').val();

    var startdate = date.split(" - ")[0];
    var enddate = date.split(" - ")[1];

    var url = encodeURIComponent("/api/v1/results/praat/get/all/date?start="+startdate+"&end="+enddate);
    $.get( "/api?url="+url ).done(function( data ) {
        renderResults(data);
        renderPageSelect(data);
    });

}

function getPatIDResults() {

    var patid = $('input[name="patidfilter"]').val();

    var url = encodeURIComponent("/api/v1/patient/get/patientnumber/" + patid);
    $.get("/api?url=" + url).done(function (data) {
        renderResults(data);
        renderPageSelect(data);
    });

}

function getPatIDDateResults() {

    var patid = $('input[name="patidfilter"]').val();
    var date = $('input[name="datefilter"]').val();

    var startdate = date.split(" - ")[0];
    var enddate = date.split(" - ")[1];

    var url = encodeURIComponent("/api/v1/results/praat/get/patid/" + patid + "/date?start="+startdate+"&end="+enddate);
    $.get("/api?url=" + url).done(function (data) {
        renderResults(data);
        renderPageSelect(data);
    });

}