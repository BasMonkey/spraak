/**
 * Created by Bas Kasemir.
 */

$( document ).ready( function () {

    $(".next-question").click( function () {
        // Get the value of the input field above the button
        var value = $( this ).prev("input").val();
        // Get the readable name of the input field above
        var readebleName = $( this ).prev("input").attr('data-readable-name');

        // Check if the field is empty. If so, show an alert and prevent the user from continuing
        if (value === '') {
            Swal.fire({
                type: 'warning',
                title: readebleName + ' ' + analyse.required_field,
                text: analyse.required_msg,
                showCancelButton: false,
                footer: '<p class="copy">'+analyse.self_closing+'</p>',
                timer: 5000
            });
            mayGoFuther = false;
            return;
        }
        // Hide the current and show the next question
        $( this ).parent(".question").hide();
        $( this ).parent().next(".question").show();
    });

    $(".previous-question").click( function () {
        var value = $( this ).prev("input").val();
        var readebleName = $( this ).prev("input").attr('data-readable-name');

        if (value === '') {
            Swal.fire({
                type: 'warning',
                title: readebleName + ' ' + analyse.required_field,
                text: analyse.required_msg,
                showCancelButton: false,
                footer: '<p class="copy">'+analyse.self_closing+'</p>',
                timer: 5000
            });
            mayGoFuther = false;
            return;
        }
        $( this ).parent(".question").hide();
        $( this ).parent(".question").prev().show();
    });

});