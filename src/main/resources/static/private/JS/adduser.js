$(document).ready( function () {

    $("#fname-field, #lname-field").keyup( function () {
        let firstname = $("#fname-field").val();
        let lastname = $("#lname-field").val();
        let substr = firstname.substr(0, 3);
        $("#username-field").val((lastname+substr).trim().toLowerCase().replace(/\s/g, ''));
        updatePreview("username-field", (lastname+substr).toLowerCase().replace(/\s/g, ''))
    });


    $('input[type="email"]').keyup(function () {
        let VAL = this.value;

        let email = new RegExp('^^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$');

        if (!email.test(VAL)) {
            $(this).addClass("invalid-form");
        } else {
            $(this).removeClass("invalid-form");
        }
    });

    $("input").blur( function () {
        if ($(this).attr("autocapitalize") === "on") {
            let value = $(this).val();
            let newval = "";
            let words = value.split(" ");
            for (let i = 0; i < words.length; i++) {
                if ( i > 0) {
                    newval += ' ';
                }

                let regex = new RegExp(
                        "(\bvan\b)|(\bder\b)|(\bden\b)|(\bde\b)|(\bhet\b)|(\bin\b)|(\b't\b)|(\baan\b)|(\bbij\b)|(\bd'\b)|(\b's\b)|(\bop\b)|(\bover\b)|(\bte\b)|(\bter\b)|(\bten\b)|(\btot\b)|(\buit\b)|(\bvoor\b)|(\buijt\b)"
                    );

                if (regex.test(words[i])) {
                    newval += words[i];
                } else {
                    newval += words[i].substr(0,1).toUpperCase()+words[i].substr(1);
                }
            }
            $(this).val(newval);
            let id = $(this).attr("id");
            updatePreview(id, newval)
        }
    }).keyup( function () {
        let value = $(this).val();
        let id = $(this).attr("id");
        updatePreview(id, value)
    }).change( function () {
        let value = $(this).val();
        let id = $(this).attr("id");
        updatePreview(id, value);
    });

    $("select").change( function () {
        let id = $(this).attr("id");
        let selector = '#' + id + ' option:selected';
        let value = $(selector).text();
        updatePreview(id, value);
    });

    function updatePreview(id, value) {
        let selector = "#"+id.split("field")[0]+"preview";
        if (value === "") {
            value = "-";
        }
        $(selector).text(value);
    }

});