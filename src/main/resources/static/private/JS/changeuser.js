
let editingself = false;

$( document ).ready( function () {

    $("#edit-user-self-btn").click( function () {
        if (!editingself) {
            enableSelfForm();
            $(this).html('<i class="fas fa-save"></i>&nbsp;Save').attr("id", 'save-user-btn');
            editingself = true;
        } else {
            editingself = false;
            cobalt.make({
                type: "loading",
                title: "Saving",
                data: "Please hold on while the user is being saved"
            });
            sendSelfForm();
        }

    });

});

function enableSelfForm() {
    $('input[data-editable-self="true"]').each( function () {
        $(this).removeAttr("readonly");
    });

    $('select[data-editable-self="true"]').each( function () {
        $(this).prev("input").hide();
        $(this).show();
    });
}

function sendSelfForm() {
    //Get the CSRF token and header to include them in the XHR request
    let token = $("meta[name='_csrf']").attr("content");
    let header = $("meta[name='_csrf_header']").attr("content");

    // Make a new formdata
    let myFormData = new FormData();

    let buttonHTML = $("#save-user-btn").html();

    $("#save-user-btn").addClass('cur-wait').html('<i class="fas fa-hourglass"></i>&nbsp;'+analyse.uploading + ', ' + analyse.please_wait);

    // Add all the entered data to the formData object.
    myFormData.append("username", $('*[name="username"]').val().toString());
    myFormData.append("fname", $('*[name="fname"]').val().toString());
    myFormData.append("lname", $('*[name="lname"]').val().toString());
    myFormData.append("email", $('*[name="email"]').val().toString());
    myFormData.append("institute", $('*[name="institute"]').children("option:selected").val().toString());
    myFormData.append("role", $('*[name="role"]').children("option:selected").val().toString());
    myFormData.append("language", $('*[name="language"]').children("option:selected").val().toString());
    myFormData.append("picture", $('img.profile-picture').attr("src").toString());

    // Make an Ajax request and handle success / errors
    $.ajax({
        url: '/user/update',
        type: 'POST',
        data: myFormData,
        processData: false,
        contentType: false,
        headers: {
            'X-CSRF-TOKEN': token
        },
        dataType: 'text',
        success: function (data) {

            if (data === "Updated") {

                console.info(data);

                cobalt.make({
                    type: "success",
                    title: "Success",
                    data: "The user has been saved :)<br><br>You may reload the page now"
                });

            } else {

                cobalt.make({
                    type: "error",
                    title: "Uh Oh",
                    data: 'An error occured: ' + xhr.status + ' ' + xhr.statusText + ' | Data: '+ data + ' | Timestamp: '+ new Date()
                });

            }
        },
        error: function(xhr) {
            cobalt.make({
                type: "error",
                title: "Uh Oh",
                data: 'An error occured: ' + xhr.status + ' ' + xhr.statusText + ' | Timestamp: '+ new Date()
            });

        }
    });
}