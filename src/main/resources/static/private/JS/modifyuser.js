$(document).ready( function () {

    dataobj = null;

    $("#username-filter").keyup( function () {
        getusers();
    });

    $('#numPerPage').change( function () {
        renderFilePageSelect(dataobj);
        renderFileResults(dataobj);
    });

});


function getusers() {

    var username = $('input[name="usernamefilter"]').val();
    var url = encodeURIComponent("usernames/" + username);

    $.get( "user/modify?url=" + url ).done(function( data ) {
        renderFileResults(data);
        renderFilePageSelect(data);
    });

}