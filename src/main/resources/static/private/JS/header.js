/**
 * Created by Bas Kasemir.
 */

$(document).ready( function () {

    var accountMenuShow = false;

    /*$(".header > .account").click(function () {

        if (accountMenuShow) {
            $(".ha-fa-caret").removeClass("upsdwn");
            $(".header .dropdown").hide();
            accountMenuShow = false;
        } else {
            $(".ha-fa-caret").addClass("upsdwn");
            $(".header .dropdown").show();
            accountMenuShow = true;
        }

    });*/

    $(document).click(function(event) {

        if (accountMenuShow) {

            var $target = $(event.target);
            if ( !$target.closest('.header .dropdown').length &&
                $('.header > .dropdown').is(":visible") ) {
                $(".ha-fa-caret").removeClass("upsdwn");
                $('.header > .dropdown').hide();
                accountMenuShow = false;
            }
        } else {
            var $target = $(event.target);
            if ($target.closest('.header .account').length  &&
                $('.header > .dropdown').is(":hidden") ) {
                $(".ha-fa-caret").addClass("upsdwn");
                $('.header > .dropdown').show();
                accountMenuShow = true;
            }
        }
    });


    /*$(document).click( function(e){

        console.log($(e.target).parents("account"));

        if ($(e.target).parents(".account") > 0) {
            if (accountMenuShow) {
                $(".ha-fa-caret").removeClass("upsdwn");
                $(".header .dropdown").hide();
                accountMenuShow = false;
            } else {
                $(".ha-fa-caret").addClass("upsdwn");
                $(".header .dropdown").show();
                accountMenuShow = true;
            }
        } else {

            $(".ha-fa-caret").removeClass("upsdwn");
            $(".header .dropdown").hide();
            accountMenuShow = false;

        }
    });*/

    $(".hamburger").click( function () {
        $(".menubar").toggle();
    });


    $("#logoutbutton").click(function () {
        $("#logoutform").submit();
    });

});