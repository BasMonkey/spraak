// Copyright 2020 Bas Kasemir

$( document ).ready( function () {

    $(".accordion-title").click(function () {
        $( this ).toggleClass("rotate");
        $( this ).next(".accordion").toggleClass("collapsed");
    });

});