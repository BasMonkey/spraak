// Copyright 2020 Bas Kasemir

/**
 * Tabs is a mini library for adding tabs to a webpage.
 */

$( document ).ready( function () {

    let tabscount = 0;

    // Get all the tab contents
    $(".tabs-content").each( function () {
        // Hide the contents
        $(this).hide();
        // Get the name of the tab
        let tabname = $(this).attr("data-tabs-name");
        // Check if it is the first tab so we can apply the active class and shwo the contents of the first tab
        tabscount++;
        let active = '';
        if (tabscount === 1) {
            active = ' active';
            $(this).show();
        }
        // Append the tab
        $(".tabs-head").append('<div class="item'+active+'">'+tabname+'</div>');
    });

    $(".tabs-head > .item").click( function () {
        let tabname = $(this).text();
        let selector = '.tabs-content[data-tabs-name="'+tabname+'"]';
        $(".tabs-head > .item").removeClass("active");
        $(this).addClass("active");
        $(".tabs-content").hide();
        $(selector).show();
    });

    $(".tabs-next").click( function () {
        $(this).parent(".tabs-content").hide().next(".tabs-content").show();
        $(".tabs-head > .item.active").removeClass("active").next(".item").addClass("active");
    });

    $(".tabs-prev").click( function () {
        $(this).parent(".tabs-content").hide().prev(".tabs-content").show();
        $(".tabs-head > .item.active").removeClass("active").prev(".item").addClass("active");
    });

});