// Copyright 2020 Marcel Zandberg

var patids = [];
var patnr = [];
var channelL = 0;
var channelR = 0;


$(document).ready(  function () {


    var pat = "";

    var patsize = 0;

    var filedataobj = null;

    var channel = "";

    //btn for retrieving the checkboxes and downloading the audiofiles
    $("#filetab").click( function () {
        getAllChecked();
    });


    //gets all checked checkboxes from selection with the id's
    async function getAllChecked() {
        //get array of checked checkboxes
        $(' .patidchecker > input[type=checkbox]').each(function (i) {

            if (this.checked) {
                patids.push($(this).attr("data-patid"));
                patsize++;

                pat = $(this).attr("data-patnr");
                if(!patnr.includes(pat)){
                    patnr.push(pat)
                }

                channel = $(this).attr("data-channel");
                if(channel === "L"){
                    channelL++;
                }else {
                    channelR++;
                }
            }
        });

        //pat count
        $("#selected_patients").text(patsize);


        //icon for whene patient over time is selected
        if(patnr.length < 2){
            $("#selected_control").html('<i class="fas fa-times-circle dogwood_blossom_red_text"></i>')
        }


    }


});

