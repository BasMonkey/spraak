// Copyright 2019 Bas Kasemir

var result_id = 0;

$(document).ready( function () {

    var parsedUrl = new URL(window.location.href);
    var result_id_array = parsedUrl.pathname.split("/");
    result_id = result_id_array[result_id_array.length - 1];

    getAllResults();

});

function renderResultObject(data) {

    console.log(data);

    if (data.length < 1) {
        swal("It seems like you cannot view this result :(");
        return;
    }

    var result = data[0];

    $("#patid-data").text(result.patientNumber);
    $("#date-data").text(result.date);
    $("#soundfilename-data").text(result.soundFile+".wav");
    $("#owner-data").text(result.userName);

    $("#splitchannels-data").text("Yes");
    $("#cutaudio-data").text("No");
    $("#normalize-data").text("Yes");
    $("#praatscriptid-data").text("1");

    $("#nsyll-data").text(result.nsyll);
    $("#npause-data").text(result.npause);
    $("#duration-data").text(result.duration);
    $("#phonationtime-data").text(result.phonationTime);

    $("#speechrate-data").text(result.speechRate);
    $("#articulation-data").text(result.articulation);
    $("#asd-data").text(result.asd);

}

/*******************
 * FETCH FUNCTIONS *
 *******************/

function getAllResults() {

    var url = encodeURIComponent("/api/v1/results/praat/get/id/"+result_id);
    $.get( "/api?url="+url ).done(function( data ) {
        renderResultObject(data);
    });

}