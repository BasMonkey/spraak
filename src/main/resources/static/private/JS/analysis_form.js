/**
 * Created by Bas Kasemir.
 */

var unloadmsg;

$(window).bind('beforeunload', function(){
    return unloadmsg;
});

$(document).ready( function () {

    unloadmsg = analyse.sure_exit;

    // ***********
    // RANDOM HASH
    // ***********

    function makeid() {
        // initalize a new variable that will contain the randomhash string
        var text = "";
        // Store all the allowed characters in a string
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        // Repeat 15 times, add a random character to the string
        for (var i = 0; i < 15; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        // Return the random hash
        return text;
    }

    // ********
    // SWITCHES
    // ********

    $("#fill-date").click( function () {

        // Get the value of the checkbox thats is linked to the fill date switch
        var val = $("#input-fill-date").val();

        // Check if the switch is turned on
        if (val > 0) {
            // Fill in the date in each corresponding input field
            $("#day").val(day);
            $("#month").val(month);
            $("#year").val(year);
            // Enable the next section button
            $("#futher-button-date").removeClass('hideonload').addClass("i-b");
            // Fill in the date on the check box on the last section
            $("#check-date").text($("#day").val()+'-'+$("#month").val()+'-'+$("#year").val());
            // Set the check to true
            mayGoFuther = true;
        } else {
            // Empty all the date input boxes
            $("#day").val("");
            $("#month").val("");
            $("#year").val("");

            // Hide the continue button
            $("#futher-button-date").removeClass("i-b").addClass('hideonload');
            // Set the check to true
            mayGoFuther = false;
        }

    });


    $('.goToMaxLen').keyup(function (e) {
        // get # of chars in the input field
        var length = $(this).val().length;
        // get maxlength value
        var maxlen = $(this).attr('maxlength');

        // when backspace is pressed check if the cursor needs to move to the previous input field
        if (e.keyCode === 8) {
            if (length == 0 && !$(this).hasClass( "dontgoback" )) {
                $(this).prev().focus();
            }
        } else {
            if (length == maxlen) {
                // if the max length is reached, set the focus on the next input
                $(this).next().focus();
            }
        }
    });

    // On every keystroke / change in the patient ID input field, update the check table with the typed value
    $("#patid").keyup( function () {
        $("#check-patid").text($(this).val());
    }).change( function () {
        $("#check-patid").text($(this).val());
    });

    $("#checkPraatSettings").click(function () {

        // Get the values of the checkboxes that are linked to the switches
        var norm_val = $("#input-normalize").val();
        var split_val = $("#input-split_channels").val();
        var cut_val = $("#input-cut_audio").val();

        // Check the value of the checkbox and fill the check table value to the right icon
        if (norm_val > 0) {
            $("#check-normalise").html('<i class="fas fa-check-circle download_green_text"></i>');
        } else {
            $("#check-normalise").html('<i class="fas fa-times-circle dogwood_blossom_red_text"></i>');
        }

        // Check the value of the checkbox and fill the check table value to the right icon
        if (split_val > 0) {
            $("#check-split").html('<i class="fas fa-check-circle download_green_text"></i>');
        } else {
            $("#check-split").html('<i class="fas fa-times-circle dogwood_blossom_red_text"></i>');
        }

        // Check the value of the checkbox and fill the check table value to the right icon
        if (cut_val > 0) {
            $("#check-cut").html('<i class="fas fa-check-circle download_green_text"></i>');
        } else {
            $("#check-cut").html('<i class="fas fa-times-circle dogwood_blossom_red_text"></i>');
        }
    });

    // ***********************
    // DROPBOX AND AJAX REQUEST
    // ***********************
        //Get the CSRF token and header to include them in the XHR request
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");

        // Make a new formdata
        var soundfileData = new FormData();

        var selected_files = 0;

        // The following functions prevent the file from being displayed when dragged in the viewport
        // They also initiate a color change when a file is hovering
        $("#dropbox").on("dragenter", function( event ){
            $(".dropbox").stop().addClass("ondrag");
            event.preventDefault();
        }).on("dragover", function( event ){
            $(".dropbox").stop().addClass("ondrag");
            event.preventDefault();
        }).on("dragleave", function( event ){
            $(".dropbox").stop().removeClass("ondrag");
            event.preventDefault();
        });

        // When user drops a file, do the following
        $("#dropbox").on("drop", function( e ) {
            // Prevent the browser from opening the file
            e.preventDefault();
            // Check if the maximum number of files is reached, if so, do nothing
            if ($( this ).hasClass("fileselect-full")) {
                return;
            }
            // Remove the styling that appears when you drag a file on to the box
            $(".dropbox").stop().removeClass("ondrag");
            // Get the dragged content as objects
            e.dataTransfer = e.originalEvent.dataTransfer;
            var files = e.target.files || e.dataTransfer.files;

            // Iterate over each file
            for (var i = 0; i < files.length; i++) {

                // Check if the file has a valid filetype. If not, show a message and do nothing else.
                if (!isValidFileType(files[i].type)) {
                    $("#wrong_filetype").show();
                    return
                } else if (selected_files >= 2) {
                    // Check if the number of total selected files has been reached.
                    // If so, run the function that disables the dropbox.
                    setOkayFiles( true );
                    return
                } else {
                    // If everything is okay, append the file to a formdata object.
                    soundfileData.append("files[]", files[i]);
                    $("#wrong_filetype, #max_filecount, #max_fileselect").hide();
                    // Append to list under the dropbox.
                    $("#filelist > ul").append('<li><i class="fas fa-file"></i>&nbsp;' + files[i].name + '</li>');
                    // Increase the number of total selected files
                    selected_files ++;
                    // Check if the number of total selected files has been reached.
                    // If so, run the function that disables the dropbox.
                    if (selected_files >= 2) {
                        setOkayFiles(false);
                    }
                }
            }
        }).click( function () {
            // When a user clicks on the box, prevent the default browser action and set the focus to the file input
            // field, so the default file selector will pop up.
            $(this).preventDefault;
            $('#files-dropbox').focus();
        });

        function setOkayFiles( showWarning ) {

            if (showWarning) {
                $("#max_filecount").show();
            }
            $( "#dropbox" ).addClass("fileselect-full");
            $( 'label[for=""]' )
            $("#audiofiles .next-form-step").show();

        }

        function isValidFileType( filetype ) {

            /*var regex = new RegExp('(TASCAM_\\d+S\\d{2})(D06\\.wav)|(TASCAM_\\d+S\\d{2})(\\.wav)');

            // Test the regex. If it matches, show an alert and reset the field.
            if (!regex.test(fn)) {
                $(selector).text("Click here to select").removeClass("small-font");
                $(this).val('');
                alert('Wrong file name.\nSee the help section for details.');
            }*/

            var regexfiletype = new RegExp('(audio\\/wav)|(audio\\/x-wav)');

            // Test the regex. If it matches, show an alert and reset the field.
            if (!regexfiletype.test(filetype)) {
                return false
            } else {
                return true
            }
        }

        function resetFiles() {
            // Delete the filesarray from the formdata object.
            soundfileData.delete("files[]");
            // Clear the filelist preview.
            $("#filelist > ul").html("");
            // Hide all error / warning messages.
            $("#wrong_filetype, #max_filecount, #max_fileselect").hide();
            // Set the dropbox to active.
            $( "#dropbox" ).removeClass("fileselect-full");
            // Set the number of total selected files to zero.
            selected_files = 0;
            // Prevent the user from going further by setting the maximum section number to the current section.
            maxSectionNum = 1;
            // Hide the continue button
            $('#fileNextBtn').addClass('hideonload-important');
        }

        // On click event for the reset files button.
        $('#resetfiles').click(function () {
            resetFiles();
        });

        // The user can click on the text (which is a label linked to the file input field) to invoke the browsers
        // file selctor. To prevent the user from adding more than two files via this, we need to check if the maximum
        // of selected files has been reached. If so, it will show the corresponding error / warning message and won't
        // do the the normal actions.
        $('label[for="files-dropbox"]').click(function (e) {
            if (selected_files >= 2) {
                e.preventDefault();
                $("#max_filecount").show();
                return
            }
        });

        // This is almost the same code as for the on file drop on line 135 - 175. See the documentation there.
        $("#files-dropbox").change( function () {
            console.log("blur");
            var ins = this.files.length;

            if (selected_files > 2) {
                $("#max_filecount").show();
                return
            } else if (ins > 2) {
                $("#max_fileselect").show();
                return
            } else {
                for (var x = 0; x < ins; x++) {
                    if (selected_files < 2) {
                        if (!isValidFileType(this.files[x].type)) {
                            $("#wrong_filetype").show();
                            return
                        }
                        var newMax = $("#filelist").next().attr('data-setMaxSection');
                        window.maxSectionNum = newMax;
                        mayGoFuther = true;
                        soundfileData.append("files[]", this.files[x]);
                        $("#wrong_filetype, #max_filecount, #max_fileselect").hide();
                        $("#filelist > ul").append('<li><i class="fas fa-file"></i>&nbsp;' + this.files[x].name + '</li>');
                        $("#check-audiofiles").append(this.files[x].name+',&nbsp;');
                        selected_files++;
                        if (selected_files >= 2) {
                            $( "#dropbox" ).addClass("fileselect-full");
                            $("#audiofiles .next-form-step").show();
                            $('#fileNextBtn').removeClass('hideonload-important');
                        }



                    } else {
                        $("#max_filecount").show();
                        return
                    }
                }
            }
        });

        // On change event handler for the date input fields.
        $(".dateinput").change(function () {
            checkDateFields();
        }).keyup(function () {
            checkDateFields();
        });

        /*$("#futher-button-date").click( function (e) {
            e.preventDefault();
            if (day >=  $("#day").val() && month >=  $("#month").val() && year >=  $("#year").val()) {
                Swal.fire({
                    type: 'success',
                    title: analyse.success,
                    text: analyse.being_processed,
                    footer: '<p class="copy">Data: ' + data + ' | Timestamp: ' + new Date() + '</p>',
                    showCancelButton: true,
                    focusConfirm: false,
                    confirmButtonColor: spraak_colors.royal_blue,
                    cancelButtonColor: spraak_colors.thundercloud_grey,
                    confirmButtonText: analyse.results_page,
                    cancelButtonText: analyse.another_one
                })
            }
        });*/

        function checkDateFields() {
            var values = $("#day").val()+$("#month").val()+$("#year").val();
            // Check if the number of filled characters equals a valid date
            if (values.length == 8) {
                // Set the filled date as value in the check table.
                $("#check-date").text($("#day").val()+'-'+$("#month").val()+'-'+$("#year").val());
                // Show the continue button
                $("#futher-button-date").removeClass('hideonload').addClass("i-b");
                // Set the check to true
                mayGoFuther = true;
            } else {
                // Hide the continue button
                $("#futher-button-date").removeClass("i-b").addClass('hideonload');
                // Set the check to true
                mayGoFuther = false;
            }
        }

        let patientExists = false;

        $("#patientID").keyup( function () {
            let patIDvalue = $(this).val().toLowerCase();
            var url = encodeURIComponent("/api/v1/patient/get/patientnumber/"+patIDvalue);
            $.get( "/api?url="+url ).done(function( data ) {
                console.log(data);
                if (data.id > 0) {
                    patientExists = true;
                    // Set the patient details in the patients table
                    $.each(data, function (key, val) {
                        let selector_pat = '#pat-'+key;

                        console.log(selector_pat);
                        $(selector_pat).text(val);
                    });
                    // Set the patient details in the check table
                    $.each(data, function (key, val) {
                        let selector_pat = '#check-'+key;
                        $(selector_pat).text(val);
                    });
                    $("#existingPatient").show();
                    $("#newPatient").hide();
                } else {
                    patientExists = false;
                    $("#existingPatient").hide();
                    $("#newPatient").show();
                }

            });
        });



        $('*[data-setMaxSection="4"], *[data-section-index="4"]').click( function() {
            if (!patientExists) {
                // Set new patient data to the check table
                $("#check-patient_number").text($("#patientID").val().toString());
                $("#check-sex").text($('input[name="pat-sex"]:checked').val().toString());
                $("#check-birthdate").text($('input[name="pat-birthdate"]').val().toString());
                $("#check-degree").text($('*[name="pat-degree"]').children("option:selected").val().toString());
                $("#check-diag_group").text($('*[name="pat-diag_group"]').children("option:selected").val().toString());
                $("#check-added_by").text(analyse.by_you + '(' + analyse.this_username + ')');
                $("#check-added_at").text(analyse.not_yet);
            }

            $("#check-channel").text($('input[name="channel"]:checked').val().toString());
            $("#check-interviewer").text($("#interviewer").val().toString());

        } );

        $("#uploadButton").click( function(e) {
            // Prevent the default browser action
            e.preventDefault();

            // Define the default error callback function
            function xhrError(xhr) {
                $(".form-progress-bar div").css('background-color', 'red');
                Swal.fire({
                    type: 'error',
                    title: analyse.error,
                    text: analyse.error_msg,
                    footer: '<p class="copy">An error occured: ' + xhr.status + ' ' + xhr.statusText + ' | Timestamp: '+ new Date() + '</p>',
                    showCancelButton: false,
                    focusConfirm: false,
                    confirmButtonColor: spraak_colors.royal_blue,
                    confirmButtonText: analyse.try_again
                }).then(function(result) {
                    // Restore the original submit button data
                    $("#uploadButton").removeClass('cur-wait').html(original_btn_data);
                    window.location.reload();
                });

            }

            // Get the original data of the submit button (so we can replace it with the right icon and locale when an
            // error occured.
            var original_btn_data = $(this).html();

            // Prevent the user from getting to another section
            maxSectionNum = -1;

            // Replace the button text with a loading message
            $(this).addClass('cur-wait').html('<i class="fas fa-hourglass"></i>&nbsp;'+analyse.uploading + ', ' + analyse.please_wait);

            // Check if the patient exists in the database, if not, save the patient first.
            if (!patientExists) {

                // Generate a patient JSON object
                let patientObject = {};
                patientObject["patient_number"] = $("#patientID").val().toString();
                patientObject["sex"] = $('input[name="pat-sex"]:checked').val().toString();
                patientObject["birthdate"] = $('input[name="pat-birthdate"]').val().toString();
                patientObject["degree"] = $('*[name="pat-degree"]').children("option:selected").val().toString();
                patientObject["diag_group"] = $('*[name="pat-diag_group"]').children("option:selected").val().toString();

                // Make a String of the JSON object
                let patientJson = JSON.stringify(patientObject);

                // Generate new empty form data
                let patientData = new FormData();

                // Append the JSON string to the formData
                patientData.append("formData", patientJson);

                // Show loading popup
                cobalt.make({
                    type: "loading",
                    title: "Saving the patient",
                    data: "This should not take long..."
                });

                // Define the API endpoint for saving the patients and encode it
                let apipatsaveurl = encodeURIComponent("/api/v1/patient/save");

                // Make a AJAX POST request to save the patients
                $.ajax({
                    url: '/api/save?url='+apipatsaveurl,
                    data: patientData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': token
                    },
                    success: function(data){
                        console.log("data patient: "+data);
                    }
                });

            } // End patients exists block




            // Generate a soundfile JSON object
            let soundfileObject = {};
            soundfileObject["patient_number"] = $("#patientID").val().toString();
            soundfileObject["date"] = $("#year").val().toString()+"-"+$("#month").val().toString()+"-"+$("#day").val().toString();
            soundfileObject["channel"] = $('input[name="channel"]:checked').val().toString();
            soundfileObject["interviewer"] = $("#interviewer").val().toString();

            // Make a String of the JSON object
            let soundfileJson = JSON.stringify(soundfileObject);

            // Add all the entered data to the formData object.
            soundfileData.append("formData", soundfileJson);

            // Show a loading popup
            cobalt.make({
                type: "loading",
                title: "Saving the soundfiles",
                data: "This should not take long..."
            });

            // Make an Ajax request and handle success / errors
            let apipatsaveurl = encodeURIComponent("/api/v1/sound/save");

            $.ajax({
                url: '/api/save?url='+apipatsaveurl,
                type: 'POST',
                data: soundfileData,
                processData: false,
                contentType: false,
                headers: {
                    'X-CSRF-TOKEN': token
                },
                dataType: 'text',
                success: function (xhr, data) {

                    cobalt.destroy();

                    console.log("SF Data: "+data)

                    if (data === "success") {

                        console.info(data);

                        Swal.fire({
                            type: 'success',
                            title: analyse.success,
                            text: analyse.being_processed,
                            footer: '<p class="copy">Data: ' + data + ' | Timestamp: ' + new Date() + '</p>',
                            showCancelButton: true,
                            focusConfirm: false,
                            confirmButtonColor: spraak_colors.royal_blue,
                            cancelButtonColor: spraak_colors.thundercloud_grey,
                            confirmButtonText: analyse.results_page,
                            cancelButtonText: analyse.another_one
                        }).then(function (result) {
                            if (result.value) {
                                window.location = "/results";
                            } else {
                                window.location.reload();
                            }
                        });

                    } else {
                        xhrError(xhr);
                        return
                    }

                    // Fill the progress bar
                    $(".form-progress-bar div").css('background-color', spraak_colors.royal_blue);
                    $(".form-progress-bar div").css('width', '100%');
                },
                error: function(xhr) {
                    cobalt.destroy();
                    xhrError(xhr);
                }
            });


        });






});