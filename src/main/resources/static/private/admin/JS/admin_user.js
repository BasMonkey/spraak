
let editing = false;

$( document ).ready( function () {

    $("#edit-user-btn").click( function () {
        if (!editing) {
            enableAdminForm();
            $(this).html('<i class="fas fa-save"></i>&nbsp;Save').attr("id", 'save-user-btn');
            editing = true;
        } else {
            cobalt.make({
                type: "loading",
                title: "Saving",
                data: "Please hold on while the user is being saved"
            });
            sendAdminForm();
        }

    });

    $("#activate-user-btn").click( function () {
        cobalt.make({
            type: "loading",
            title: "Saving",
            data: "Please hold on while the user is being activated"
        });
        changeActivation(true);
    });

    $("#deactivate-user-btn").click( function () {
        cobalt.make({
            type: "loading",
            title: "Saving",
            data: "Please hold on while the user is being deactivated"
        });
        changeActivation(false);
    });

    $("#delete-user-btn").click( function () {
        Swal.fire({
            type: 'warning',
            title: "Are you sure?",
            text: "Are you sure you want to delete the user?",
            showCancelButton: true,
            footer: '<p class="copy">This action can not be undone</p>',
        }).then(function(isConfirm) {
            if (isConfirm) {
                console.log("isConfirm in else: ");
                return;
            } else {
                deleteUser()

            }
        });
    });
});

function enableAdminForm() {
    $('input[data-editable-admin="true"]').each( function () {
        $(this).removeAttr("readonly");
    });

    $('select[data-editable-admin="true"]').each( function () {
        $(this).prev("input").hide();
        $(this).show();
    });
}

function changeActivation(state) {
    //Get the CSRF token and header to include them in the XHR request
    let token = $("meta[name='_csrf']").attr("content");
    let header = $("meta[name='_csrf_header']").attr("content");

    // Make a new formdata
    let myFormData = new FormData();

    // Add all the entered data to the formData object.
    myFormData.append("username", $('*[name="username"]').val().toString());

    let url;

    if (state) {
        url = "/user/activate"
    }

    if (!state) {
        url = "/user/deactivate"
    }

    // Make an Ajax request and handle success / errors
    $.ajax({
        url: url,
        type: 'POST',
        data: myFormData,
        processData: false,
        contentType: false,
        headers: {
            'X-CSRF-TOKEN': token
        },
        dataType: 'text',
        success: function (data) {

            let dataparse = JSON.parse(data);

            if (dataparse.status) {

                console.info(data);

                cobalt.make({
                    type: "success",
                    title: "Success",
                    data: "The user has been saved :)<br><br>You may reload the page now"
                });

            } else {

                cobalt.make({
                    type: "error",
                    title: "Uh Oh",
                    data: 'An error occured: ' + xhr.status + ' ' + xhr.statusText + ' | Message: '+ dataparse.message + ' | Timestamp: '+ new Date()
                });

            }
        },
        error: function(xhr) {
            cobalt.make({
                type: "error",
                title: "Uh Oh",
                data: 'An error occured: ' + xhr.status + ' ' + xhr.statusText + ' | Timestamp: '+ new Date()
            });

        }
    });

}

function deleteUser() {
    //Get the CSRF token and header to include them in the XHR request
    let token = $("meta[name='_csrf']").attr("content");
    let header = $("meta[name='_csrf_header']").attr("content");

    // Make a new formdata
    let myFormData = new FormData();

    // Add all the entered data to the formData object.
    myFormData.append("username", $('*[name="username"]').val().toString());
    // Make an Ajax request and handle success / errors
    $.ajax({
        url: "/user/delete",
        type: 'POST',
        data: myFormData,
        processData: false,
        contentType: false,
        headers: {
            'X-CSRF-TOKEN': token
        },
        dataType: 'text',
        success: function (data) {

            let dataparse = JSON.parse(data);

            if (dataparse.status) {

                console.info(data);

                cobalt.make({
                    type: "success",
                    title: "Success",
                    data: "The user has been saved :)<br><br>You may reload the page now"
                });

            } else {

                cobalt.make({
                    type: "error",
                    title: "Uh Oh",
                    data: 'An error occured: ' + xhr.status + ' ' + xhr.statusText + ' | Message: '+ dataparse.message + ' | Timestamp: '+ new Date()
                });

            }
        },
        error: function(xhr) {
            cobalt.make({
                type: "error",
                title: "Uh Oh",
                data: 'An error occured: ' + xhr.status + ' ' + xhr.statusText + ' | Timestamp: '+ new Date()
            });

        }
    });

}

function sendAdminForm() {
    //Get the CSRF token and header to include them in the XHR request
    let token = $("meta[name='_csrf']").attr("content");
    let header = $("meta[name='_csrf_header']").attr("content");

    // Make a new formdata
    let myFormData = new FormData();

    let buttonHTML = $("#save-user-btn").html();

    $("#save-user-btn").addClass('cur-wait').html('<i class="fas fa-hourglass"></i>&nbsp;'+analyse.uploading + ', ' + analyse.please_wait);

    // Add all the entered data to the formData object.
    myFormData.append("username", $('*[name="username"]').val().toString());
    myFormData.append("fname", $('*[name="fname"]').val().toString());
    myFormData.append("lname", $('*[name="lname"]').val().toString());
    myFormData.append("email", $('*[name="email"]').val().toString());
    myFormData.append("language", $('*[name="language"]').children("option:selected").val().toString());
    myFormData.append("institute", $('*[name="institute"]').children("option:selected").val().toString());
    myFormData.append("role", $('*[name="role"]').children("option:selected").val().toString());
    myFormData.append("picture", $('img.profile-picture').attr("src").toString());

    // Make an Ajax request and handle success / errors
    $.ajax({
        url: '/user/update',
        type: 'POST',
        data: myFormData,
        processData: false,
        contentType: false,
        headers: {
            'X-CSRF-TOKEN': token
        },
        dataType: 'text',
        success: function (data) {

            if (data === "Updated") {

                console.info(data);

                cobalt.make({
                    type: "success",
                    title: "Success",
                    data: "The user has been saved :)<br><br>You may reload the page now"
                });

            } else {

                cobalt.make({
                    type: "error",
                    title: "Uh Oh",
                    data: 'An error occured: ' + xhr.status + ' ' + xhr.statusText + ' | Data: '+ data + ' | Timestamp: '+ new Date()
                });

            }
        },
        error: function(xhr) {
            cobalt.make({
                type: "error",
                title: "Uh Oh",
                data: 'An error occured: ' + xhr.status + ' ' + xhr.statusText + ' | Timestamp: '+ new Date()
            });

        }
    });
}