Drop table if exists Spraak_institutes;
DROP TABLE if EXISTS Spraak_users_login_attempts;
DROP TABLE IF EXISTS Spraak_users_roles;
DROP TABLE IF EXISTS Spraak_users;

create table if not exists Spraak_users (
    id int auto_increment,
	first_name varchar(45) not null,
	last_name varchar(45) not null,
	email varchar(45) not null,
	username varchar(45) not null,
	password varchar(80) null,
	totp_secret varchar(60) not null,
	profile_picture longtext null,
	profile_language char(5) default 'us-EN' null,
	created_date datetime null,
	password_token varchar(200) null,
	password_timestamp datetime null,
	password_reset_link_valid tinyint(1) default 0 not null,
	enabled tinyint(1) null,
	institute varchar(3) null,
	primary key (id, username),
	constraint userName unique (username)
);


CREATE TABLE IF NOT EXISTS Spraak_users_roles (
  user_role_id int(11) NOT NULL AUTO_INCREMENT,
  username varchar(20) NOT NULL,
  role varchar(20) NOT NULL,
  PRIMARY KEY (user_role_id),
  UNIQUE KEY uni_username_role (role,username),
  KEY fk_username_idx (username),
  CONSTRAINT fk_username_roles FOREIGN KEY (username) REFERENCES Spraak_users (username));

create table if not EXISTS Spraak_users_login_attempts
(
    id                       int auto_increment,
    username                 varchar(45) not null,
    number_of_wrong_attempts int         null,
    primary key (id, username),
    constraint userNameLA
        unique (username)
);


create table if not exists Spraak_institutes
(
    abbreviation VARCHAR(3) not null  primary key,
    full_name    varchar(255)              null,
    api_host     varchar(255)              null
);